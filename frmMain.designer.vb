﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.MainMenu = New System.Windows.Forms.MenuStrip
        Me.tsmDane = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmPobierz = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblProduction = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblExpedition = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmWizualizacja = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmMiesieczne = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmScrapPrices = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmRozeslij = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmPomocne = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmKontrolaKompletnosci = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmZmienJezyk = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblSwieta = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblRabaty = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmkEURO = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmtblSources = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmtblKontrola = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmZmianaUstawien = New System.Windows.Forms.ToolStripMenuItem
        Me.tscmbDostepy = New System.Windows.Forms.ToolStripComboBox
        Me.tsmSzerokoscOkresu = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmKompaktuj = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator19 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmKoniec = New System.Windows.Forms.ToolStripMenuItem
        Me.ssMain = New System.Windows.Forms.StatusStrip
        Me.tspbMain = New System.Windows.Forms.ToolStripProgressBar
        Me.tsslLicznik = New System.Windows.Forms.ToolStripStatusLabel
        Me.tsslMain = New System.Windows.Forms.ToolStripStatusLabel
        Me.ttMain = New System.Windows.Forms.ToolTip(Me.components)
        Me.dtpOkres = New System.Windows.Forms.DateTimePicker
        Me.txtKomunikat = New System.Windows.Forms.TextBox
        Me.MainHelp = New System.Windows.Forms.HelpProvider
        Me.TimerOfDepData = New System.Windows.Forms.Timer(Me.components)
        Me.MainMenu.SuspendLayout()
        Me.ssMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu
        '
        Me.MainMenu.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmDane, Me.tsmPomocne})
        Me.MainMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu.Name = "MainMenu"
        Me.MainMenu.Padding = New System.Windows.Forms.Padding(5, 2, 0, 2)
        Me.MainMenu.Size = New System.Drawing.Size(641, 24)
        Me.MainMenu.TabIndex = 0
        Me.MainMenu.Text = "MenuStrip1"
        '
        'tsmDane
        '
        Me.tsmDane.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmPobierz, Me.ToolStripMenuItem3, Me.ToolStripSeparator2, Me.tsmWizualizacja, Me.tsmMiesieczne, Me.tsmScrapPrices, Me.tsmRozeslij})
        Me.tsmDane.Name = "tsmDane"
        Me.tsmDane.Size = New System.Drawing.Size(47, 20)
        Me.tsmDane.Text = "&Dane"
        '
        'tsmPobierz
        '
        Me.tsmPobierz.Name = "tsmPobierz"
        Me.tsmPobierz.Size = New System.Drawing.Size(194, 22)
        Me.tsmPobierz.Text = "Po&bierz"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmtblProduction, Me.tsmtblExpedition})
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(194, 22)
        Me.ToolStripMenuItem3.Text = "&Korekta"
        '
        'tsmtblProduction
        '
        Me.tsmtblProduction.Name = "tsmtblProduction"
        Me.tsmtblProduction.Size = New System.Drawing.Size(127, 22)
        Me.tsmtblProduction.Text = "&Produkcja"
        '
        'tsmtblExpedition
        '
        Me.tsmtblExpedition.Name = "tsmtblExpedition"
        Me.tsmtblExpedition.Size = New System.Drawing.Size(127, 22)
        Me.tsmtblExpedition.Text = "&Wysyłka"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(191, 6)
        '
        'tsmWizualizacja
        '
        Me.tsmWizualizacja.Name = "tsmWizualizacja"
        Me.tsmWizualizacja.Size = New System.Drawing.Size(194, 22)
        Me.tsmWizualizacja.Text = "&Wizualizacja"
        '
        'tsmMiesieczne
        '
        Me.tsmMiesieczne.Name = "tsmMiesieczne"
        Me.tsmMiesieczne.Size = New System.Drawing.Size(194, 22)
        Me.tsmMiesieczne.Text = "Aktualizuj &miesięczne"
        '
        'tsmScrapPrices
        '
        Me.tsmScrapPrices.Name = "tsmScrapPrices"
        Me.tsmScrapPrices.Size = New System.Drawing.Size(194, 22)
        Me.tsmScrapPrices.Text = "Aktualizuj Scrap Prices"
        '
        'tsmRozeslij
        '
        Me.tsmRozeslij.Name = "tsmRozeslij"
        Me.tsmRozeslij.Size = New System.Drawing.Size(194, 22)
        Me.tsmRozeslij.Text = "&Roześlij"
        '
        'tsmPomocne
        '
        Me.tsmPomocne.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmKontrolaKompletnosci, Me.tsmZmienJezyk, Me.ToolStripMenuItem5, Me.tsmZmianaUstawien, Me.tsmSzerokoscOkresu, Me.tsmKompaktuj, Me.ToolStripSeparator19, Me.tsmKoniec})
        Me.tsmPomocne.Name = "tsmPomocne"
        Me.tsmPomocne.Size = New System.Drawing.Size(70, 20)
        Me.tsmPomocne.Text = "Pomocne"
        '
        'tsmKontrolaKompletnosci
        '
        Me.tsmKontrolaKompletnosci.Name = "tsmKontrolaKompletnosci"
        Me.tsmKontrolaKompletnosci.Size = New System.Drawing.Size(240, 22)
        Me.tsmKontrolaKompletnosci.Text = "Ko&ntrola kompletności"
        '
        'tsmZmienJezyk
        '
        Me.tsmZmienJezyk.Name = "tsmZmienJezyk"
        Me.tsmZmienJezyk.Size = New System.Drawing.Size(240, 22)
        Me.tsmZmienJezyk.Text = "Zmień &język"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmtblSwieta, Me.tsmtblRabaty, Me.tsmkEURO, Me.ToolStripSeparator4, Me.tsmtblSources, Me.tsmtblKontrola})
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(240, 22)
        Me.ToolStripMenuItem5.Text = "Dane &stałe"
        '
        'tsmtblSwieta
        '
        Me.tsmtblSwieta.Name = "tsmtblSwieta"
        Me.tsmtblSwieta.Size = New System.Drawing.Size(174, 22)
        Me.tsmtblSwieta.Text = "świę&ta"
        '
        'tsmtblRabaty
        '
        Me.tsmtblRabaty.Name = "tsmtblRabaty"
        Me.tsmtblRabaty.Size = New System.Drawing.Size(174, 22)
        Me.tsmtblRabaty.Text = "Rabaty"
        '
        'tsmkEURO
        '
        Me.tsmkEURO.Name = "tsmkEURO"
        Me.tsmkEURO.Size = New System.Drawing.Size(174, 22)
        Me.tsmkEURO.Text = "Kurs EURO"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(171, 6)
        '
        'tsmtblSources
        '
        Me.tsmtblSources.Name = "tsmtblSources"
        Me.tsmtblSources.Size = New System.Drawing.Size(174, 22)
        Me.tsmtblSources.Text = "&ustawienia kolumn"
        '
        'tsmtblKontrola
        '
        Me.tsmtblKontrola.Name = "tsmtblKontrola"
        Me.tsmtblKontrola.Size = New System.Drawing.Size(174, 22)
        Me.tsmtblKontrola.Text = "&kontrolne"
        '
        'tsmZmianaUstawien
        '
        Me.tsmZmianaUstawien.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tscmbDostepy})
        Me.tsmZmianaUstawien.Name = "tsmZmianaUstawien"
        Me.tsmZmianaUstawien.Size = New System.Drawing.Size(240, 22)
        Me.tsmZmianaUstawien.Text = "Zmiana ustawień:"
        '
        'tscmbDostepy
        '
        Me.tscmbDostepy.DropDownWidth = 150
        Me.tscmbDostepy.Items.AddRange(New Object() {"podpisu", "adresu bazy danych", "listy dystrybucyjnej", "listy poniedziałkowej", "adresu CHO DB"})
        Me.tscmbDostepy.Name = "tscmbDostepy"
        Me.tscmbDostepy.Size = New System.Drawing.Size(121, 23)
        '
        'tsmSzerokoscOkresu
        '
        Me.tsmSzerokoscOkresu.Name = "tsmSzerokoscOkresu"
        Me.tsmSzerokoscOkresu.Size = New System.Drawing.Size(240, 22)
        Me.tsmSzerokoscOkresu.Text = "&Szerokość wyświetlania okresu"
        '
        'tsmKompaktuj
        '
        Me.tsmKompaktuj.Name = "tsmKompaktuj"
        Me.tsmKompaktuj.Size = New System.Drawing.Size(240, 22)
        Me.tsmKompaktuj.Text = "Ko&mpaktuj dane"
        '
        'ToolStripSeparator19
        '
        Me.ToolStripSeparator19.Name = "ToolStripSeparator19"
        Me.ToolStripSeparator19.Size = New System.Drawing.Size(237, 6)
        '
        'tsmKoniec
        '
        Me.tsmKoniec.Name = "tsmKoniec"
        Me.tsmKoniec.Size = New System.Drawing.Size(240, 22)
        Me.tsmKoniec.Text = "&Koniec"
        '
        'ssMain
        '
        Me.ssMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tspbMain, Me.tsslLicznik, Me.tsslMain})
        Me.ssMain.Location = New System.Drawing.Point(0, 372)
        Me.ssMain.Name = "ssMain"
        Me.ssMain.Padding = New System.Windows.Forms.Padding(1, 0, 11, 0)
        Me.ssMain.Size = New System.Drawing.Size(641, 26)
        Me.ssMain.TabIndex = 1
        Me.ssMain.Text = "StatusStrip1"
        '
        'tspbMain
        '
        Me.tspbMain.AutoSize = False
        Me.tspbMain.Name = "tspbMain"
        Me.tspbMain.Size = New System.Drawing.Size(80, 20)
        '
        'tsslLicznik
        '
        Me.tsslLicznik.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.tsslLicznik.Name = "tsslLicznik"
        Me.tsslLicznik.Size = New System.Drawing.Size(0, 21)
        Me.tsslLicznik.Visible = False
        '
        'tsslMain
        '
        Me.tsslMain.AutoSize = False
        Me.tsslMain.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner
        Me.tsslMain.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsslMain.Name = "tsslMain"
        Me.tsslMain.Size = New System.Drawing.Size(350, 21)
        Me.tsslMain.Text = "Gotowe"
        Me.tsslMain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpOkres
        '
        Me.dtpOkres.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpOkres.CustomFormat = "   yyyy-MM-dd"
        Me.dtpOkres.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right
        Me.dtpOkres.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.dtpOkres.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpOkres.Location = New System.Drawing.Point(507, 0)
        Me.dtpOkres.Margin = New System.Windows.Forms.Padding(2)
        Me.dtpOkres.MinDate = New Date(2013, 1, 1, 0, 0, 0, 0)
        Me.dtpOkres.Name = "dtpOkres"
        Me.dtpOkres.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtpOkres.Size = New System.Drawing.Size(125, 22)
        Me.dtpOkres.TabIndex = 6
        Me.ttMain.SetToolTip(Me.dtpOkres, "Obowiązująca data przetwarzania")
        '
        'txtKomunikat
        '
        Me.txtKomunikat.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtKomunikat.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtKomunikat.Location = New System.Drawing.Point(166, 129)
        Me.txtKomunikat.Margin = New System.Windows.Forms.Padding(2)
        Me.txtKomunikat.Multiline = True
        Me.txtKomunikat.Name = "txtKomunikat"
        Me.txtKomunikat.ReadOnly = True
        Me.txtKomunikat.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtKomunikat.Size = New System.Drawing.Size(28, 175)
        Me.txtKomunikat.TabIndex = 5
        Me.txtKomunikat.Visible = False
        '
        'MainHelp
        '
        Me.MainHelp.HelpNamespace = "C:\CHO Monitoring\Help\CHO_Monitor_Dzienny.chm"
        '
        'TimerOfDepData
        '
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(641, 398)
        Me.Controls.Add(Me.dtpOkres)
        Me.Controls.Add(Me.txtKomunikat)
        Me.Controls.Add(Me.ssMain)
        Me.Controls.Add(Me.MainMenu)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MainMenu
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(650, 426)
        Me.Name = "frmMain"
        Me.MainHelp.SetShowHelp(Me, True)
        Me.Text = "CHO Monitoring dzienny"
        Me.MainMenu.ResumeLayout(False)
        Me.MainMenu.PerformLayout()
        Me.ssMain.ResumeLayout(False)
        Me.ssMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MainMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents tsmDane As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ssMain As System.Windows.Forms.StatusStrip
    Friend WithEvents tspbMain As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents tsslMain As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cmbMiesiace As System.Windows.Forms.ComboBox
    Friend WithEvents tsmPobierz As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ttMain As System.Windows.Forms.ToolTip
    Friend WithEvents tsmPomocne As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsslLicznik As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtKomunikat As System.Windows.Forms.TextBox
    Friend WithEvents tsmKoniec As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MainHelp As System.Windows.Forms.HelpProvider
    Friend WithEvents ToolStripSeparator19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmWizualizacja As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmKompaktuj As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmMiesieczne As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmRozeslij As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmZmienJezyk As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblProduction As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblExpedition As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblSwieta As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dtpOkres As System.Windows.Forms.DateTimePicker
    Friend WithEvents tsmZmianaUstawien As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tscmbDostepy As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmtblSources As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmKontrolaKompletnosci As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TimerOfDepData As System.Windows.Forms.Timer
    Friend WithEvents tsmtblKontrola As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSzerokoscOkresu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmScrapPrices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmkEURO As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmtblRabaty As System.Windows.Forms.ToolStripMenuItem

End Class
