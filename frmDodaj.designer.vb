﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDodaj
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.OK_Button = New System.Windows.Forms.Button
        Me.cmbPole = New System.Windows.Forms.ComboBox
        Me.ttMain = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtKwota = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.cmbZaklad = New System.Windows.Forms.ComboBox
        Me.cmbSklad = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.OK_Button.Location = New System.Drawing.Point(314, 168)
        Me.OK_Button.Margin = New System.Windows.Forms.Padding(4)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(80, 32)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "Dodaj"
        '
        'cmbPole
        '
        Me.cmbPole.FormattingEnabled = True
        Me.cmbPole.Location = New System.Drawing.Point(135, 84)
        Me.cmbPole.Name = "cmbPole"
        Me.cmbPole.Size = New System.Drawing.Size(259, 26)
        Me.cmbPole.TabIndex = 1
        '
        'ttMain
        '
        Me.ttMain.ToolTipTitle = "Wybierz nazwę kolumny do zmiany wartości"
        '
        'txtKwota
        '
        Me.txtKwota.Location = New System.Drawing.Point(135, 116)
        Me.txtKwota.MaxLength = 20
        Me.txtKwota.Name = "txtKwota"
        Me.txtKwota.Size = New System.Drawing.Size(160, 24)
        Me.txtKwota.TabIndex = 2
        Me.txtKwota.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ttMain.SetToolTip(Me.txtKwota, "Liczba zostanie dodana do dotychczasowej")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 119)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 18)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Do dodania"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 18)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Skład / ITEM"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 18)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Zakład"
        '
        'cmbZaklad
        '
        Me.cmbZaklad.FormattingEnabled = True
        Me.cmbZaklad.Location = New System.Drawing.Point(135, 20)
        Me.cmbZaklad.Name = "cmbZaklad"
        Me.cmbZaklad.Size = New System.Drawing.Size(160, 26)
        Me.cmbZaklad.TabIndex = 6
        '
        'cmbSklad
        '
        Me.cmbSklad.FormattingEnabled = True
        Me.cmbSklad.Location = New System.Drawing.Point(135, 52)
        Me.cmbSklad.Name = "cmbSklad"
        Me.cmbSklad.Size = New System.Drawing.Size(259, 26)
        Me.cmbSklad.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(28, 87)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 18)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Pole"
        '
        'frmDodaj
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(418, 230)
        Me.Controls.Add(Me.OK_Button)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cmbSklad)
        Me.Controls.Add(Me.cmbZaklad)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtKwota)
        Me.Controls.Add(Me.cmbPole)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDodaj"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Dodaj do pola"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents cmbPole As System.Windows.Forms.ComboBox
    Friend WithEvents ttMain As System.Windows.Forms.ToolTip
    Friend WithEvents txtKwota As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmbZaklad As System.Windows.Forms.ComboBox
    Friend WithEvents cmbSklad As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label

End Class
