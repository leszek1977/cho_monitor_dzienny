﻿
' Wsparcie dla działań związanych z Excel-em
#Region "Opcje i referencje"

Option Explicit On
Option Strict Off
Option Compare Text
Option Infer On

Imports System.IO
Imports CM = CCenterMsg.CenterMsg
Imports CPomocne
Imports XL = Microsoft.Office.Interop.Excel

#End Region ' Opcje i referencje

' Modified 2014-09-22
Module aa4Excel

#Region "Deklaracje"

    Public screenFresh As Boolean
    Private CP As New CPomocne.Pomocne
    Private oApp As XL.Application
    Private oBook As XL._Workbook
    Private oSheet As XL._Worksheet
    Private m_Usun As Boolean = False
    Private m_Dodaj As Boolean = False
    Private m_Nazwa As String = ""
    Private m_Row As Integer = 0
    Private m_Col As Integer

#End Region ' Deklaracje

    ' Odczytuje dane z pliku HTML, wkleja do Excela i zwraca dane tabelaryczne
    'Function fHTMLToObjectViaExcel(ByVal HTMLFile As String, _
    '                               ByRef Tabela As Object, _
    '                               Optional ByVal ShowMessage As Boolean = False, _
    '                               Optional ByVal CompareDate As Boolean = False) As Boolean

    '    Dim m_Return As Boolean = False
    '    Dim sr As StreamReader
    '    Dim m_HTML As String
    '    Dim oData As New DataObject
    '    Dim oRng As XL.Range
    '    Dim i As Integer
    '    Try
    '        If Not My.Computer.FileSystem.FileExists(HTMLFile) _
    '            Then Throw New ArgumentException("Plik [" + HTMLFile + "] nie istnieje")
    '        If CompareDate AndAlso Not fCompareFileDate(HTMLFile, Today, True, , , True) _
    '            Then Throw New ArgumentException("Plik [" + HTMLFile + "] utworzony wcześniej")
    '        sr = New StreamReader(HTMLFile)
    '        m_HTML = sr.ReadToEnd
    '        oData.SetData(DataFormats.Text, m_HTML)
    '        Clipboard.SetDataObject(oData)
    '        oApp = New XL.Application
    '        oBook = oApp.Workbooks.Add()
    '        oSheet = CType(oBook.Sheets(1), XL._Worksheet)
    '        oSheet.Range("A1").Select()
    '        oSheet.Paste()
    '        oBook.Saved = True
    '        ' ustalenie obszaru
    '        i = fLastRow(oApp)
    '        oRng = oSheet.Range("B" + i.ToString).CurrentRegion
    '        ReDim Tabela(0 To oRng.Rows.Count - 1, oRng.Columns.Count - 1)
    '        Tabela = oRng.Value2
    '        m_Return = True
    '    Catch ex As Exception
    '        Using New CM
    '            If ShowMessage Then _
    '                    MessageBox.Show(ex.Message, "Błąd odczytu danych HTML", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '        End Using
    '    Finally
    '        oData = Nothing
    '        sr = Nothing
    '        oRng = Nothing
    '        XlsClean()
    '    End Try
    '    Return m_Return
    'End Function

    ' Zwraca literę(y) oznaczające kolumnę
    Function fGetColumnLetter(ByVal Kolumna As Integer) As String

        Dim div As Integer = Kolumna
        Dim _Ret As String = String.Empty
        Dim _Mod As Integer = 0
        While div > 0
            _Mod = (div - 1) Mod 26
            _Ret += Chr(65 + _Mod)
            div = CInt((div - _Mod) \ 26)
        End While
        If _Ret.Length.Equals(2) Then
            Return _Ret.Substring(1, 1) + _Ret.Substring(0, 1)
        Else
            Return _Ret
        End If
    End Function

    'Bezpośredni odczyt danych ze skoroszytu
    'Function fGetData2Array(ByVal Plik As String, _
    '                        ByVal Arkusz As String, _
    '                        Optional ByVal Obszar As String = "", _
    '                        Optional ByVal Haslo As String = "", _
    '                        Optional ByVal NazwaObszaru As String = "") As Array

    '    ' !!! Array zaczyna się od 1 w obu wymiarach!!!
    '    ' liczby jako double (0.0)
    '    Dim aDane As Array = Nothing
    '    Dim _Obszar As String
    '    Dim _Row As Integer
    '    If Not My.Computer.FileSystem.FileExists(Plik) Then Throw New ArgumentException("Plik [" + Plik + "] nie istnieje")
    '    Try
    '        oApp = New XL.Application()
    '        oBook = oApp.Workbooks.Open(Filename:=Plik, UpdateLinks:=False, ReadOnly:=True)
    '        With oBook
    '            Try
    '                oSheet = CType(.Sheets(Arkusz), XL._Worksheet)
    '            Catch ex As Exception
    '                Throw New ArgumentException("Arkusz [" + Arkusz + "]:" & vbCrLf & ex.Message)
    '            End Try
    '            With oSheet
    '                If Not String.IsNullOrEmpty(Haslo) Then .Unprotect(Haslo)
    '                If Not String.IsNullOrEmpty(NazwaObszaru) Then
    '                    aDane = CType(CType(.Range(NazwaObszaru), XL.Range).Value, Array)
    '                ElseIf String.IsNullOrEmpty(Obszar) Then
    '                    aDane = CType(CType(.Range("A1").CurrentRegion, XL.Range).Value, Array)
    '                ElseIf Not Obszar.Contains(":") Then
    '                    Obszar += ":" + fLastAddress(oApp, Arkusz)
    '                    aDane = CType(CType(.Range(Obszar), XL.Range).Value, Array)
    '                ElseIf Not "0123456789".Contains(Obszar.Substring(Obszar.Length - 1)) Then
    '                    _Obszar = Obszar.Substring(0, Obszar.IndexOf(":"))
    '                    _Row = CType(.Range(_Obszar), XL.Range).Row
    '                    _Obszar = Obszar + _Row.ToString
    '                    Obszar &= CType(.Range(_Obszar).CurrentRegion, XL.Range).Rows.Count + _Row - 2
    '                    aDane = CType(CType(.Range(Obszar), XL.Range).Value, Array)
    '                Else
    '                    aDane = CType(CType(.Range(Obszar), XL.Range).Value, Array)
    '                End If
    '                If Not String.IsNullOrEmpty(Haslo) Then .Protect(Haslo)
    '            End With
    '        End With
    '    Catch ex As Exception
    '        Using New CM
    '            MessageBox.Show(ex.Message + vbCrLf + "Plik: " + Plik + vbCrLf + " Obszar (domyślnie A1 z otoczeniem) [" + Obszar + NazwaObszaru + "]", "Odczyt danych z Excela", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        End Using
    '    Finally
    '        Try
    '            oBook.Saved = True
    '        Catch ex As Exception
    '        End Try
    '        XlsClean()
    '    End Try
    '    Return aDane
    'End Function

#Region "Kontrola plików wzorcowych i wynikowych"

    ' Wymaga istnienia ustawienia nazw folderów My.Settings.Wzorzec i .Wyniki
    Function fCreateResultFile(ByVal Wzorzec As String, _
                               ByVal Utworz As Boolean, _
                               Optional ByVal Okres As String = "", _
                               Optional ByVal Wynikowy As String = "", _
                               Optional ByVal ExtOfFile As String = "") As String

        Dim m_Ext As String = fCheckWzorzec(Wzorzec, ExtOfFile)
        Dim m_File As String
        Dim m_Path As String = fCheckWyniki()
        Dim m_Data1, m_Data2 As DateTime
        Const CO_NAG As String = "Kontrola pliku wynikowego"
        Try
            If String.IsNullOrEmpty(m_Ext) Then Throw New ArgumentException("Nieokreślona lokalizacja wzorca [" + Wzorzec + "]")
            If Not m_Path.EndsWith(Path.DirectorySeparatorChar) Then m_Path += Path.DirectorySeparatorChar
            If String.IsNullOrEmpty(Okres) Then
                If String.IsNullOrEmpty(Wynikowy) Then
                    m_File = Wzorzec + m_Ext
                Else
                    m_File = Wynikowy + m_Ext
                End If
            Else
                m_File = Wzorzec + "_" + Okres + m_Ext
            End If
            m_Data1 = My.Computer.FileSystem.GetFileInfo(My.Settings.Wzorce + Wzorzec + m_Ext).LastWriteTime
            If My.Computer.FileSystem.FileExists(m_Path + m_File) Then
                ' porównaj datę utworzenia do pliku oryginalnego
                m_Data2 = My.Computer.FileSystem.GetFileInfo(m_Path + m_File).CreationTime
                If DateTime.Compare(m_Data1, m_Data2) <= 0 Then
                    Return m_Path + m_File
                End If
            End If
            If Not Utworz Then Throw New ArgumentException("Plik [" + m_File + "] nie istnieje w lokalizacji " + m_Path)
            My.Computer.FileSystem.CopyFile(My.Settings.Wzorce + Wzorzec + m_Ext, m_Path + m_File, True)
            File.SetCreationTime(m_Path + m_File, m_Data1)
            If Not My.Computer.FileSystem.FileExists(m_Path + m_File) Then _
                Throw New ArgumentException("Błąd utworzenia pliku [" + m_File + "]")
            Return m_Path + m_File
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_NAG, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
            Return ""
        End Try
    End Function

    Function fCheckWyniki() As String

        Try
            With My.Settings
                If String.IsNullOrEmpty(.Wyniki.Trim) OrElse Not CP.CheckFolder(.Wyniki) Then
                    Dim fbd As New FolderBrowserDialog
                    With fbd
                        .RootFolder = Environment.SpecialFolder.MyComputer
                        .SelectedPath = My.Settings.Wyniki
                        If .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then
                            My.Settings.Wyniki = .SelectedPath + Path.DirectorySeparatorChar
                            My.Settings.Save()
                        Else
                            Throw New ArgumentException
                        End If
                    End With
                End If
                Return .Wyniki
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show("Błąd utworzenia katalogu danych wynikowych", "Zapisy w C:\", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Return "C:\"
        End Try
    End Function

    Function fCheckWzorzec(ByVal Nazwa As String, _
                           Optional ByVal ExtOfFile As String = "") As String

        Dim m_File As String
        Dim m_Ret As String = ""
        Try
            If String.IsNullOrEmpty(ExtOfFile) Then ExtOfFile = fGetFileExt()
            oApp = Nothing
            With My.Settings
                If Not .Wzorce.EndsWith(Path.DirectorySeparatorChar) Then
                    .Wzorce += Path.DirectorySeparatorChar
                End If
                m_File = Nazwa + ExtOfFile
                If String.IsNullOrEmpty(.Wzorce.Trim) OrElse Not My.Computer.FileSystem.FileExists(.Wzorce + m_File) Then
                    If My.Computer.FileSystem.FileExists(.Wzorce + m_File.Replace("xlsm", "xlsx")) Then
                        m_File = m_File.ToLower.Replace("xlsm", "xlsx")
                        GoTo PoWyborze
                    End If
                    Dim opf As New OpenFileDialog
                    With opf
                        .Multiselect = False
                        .InitialDirectory = My.Settings.Wzorce
                        .FileName = m_File
                        .Filter = "Plik wzorcowy (*.xl*)|*.xl*"
                        If .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then
                            My.Settings.Wzorce = .FileName.Replace(.SafeFileName, "")
                            My.Settings.Save()
                            m_File = .SafeFileName
                        Else
                            Throw New ArgumentException
                        End If
                    End With
                End If
            End With
PoWyborze:
            m_Ret = m_File.Substring(m_File.LastIndexOf("."))
        Catch ex As Exception
        Finally
            pReleaseObject(oApp)
        End Try
        Return m_Ret
    End Function

    Function fGetFileExt() As String

        oApp = New XL.Application
        Dim _Ver As Integer = Val(oApp.Application.Version)
        Dim _Ret As String = ".xlsm"
        If Val(oApp.Application.Version) < 12 Then _Ret = ".xls"
        oApp = Nothing
        Return _Ret
    End Function

#End Region ' Kontrola plików wzorcowych i wynikowych

    Private Sub AktywnyArkusz(ByVal sender As Object, _
                              Optional ByVal NazwaArkusza As String = "", _
                              Optional ByVal Skoroszyt As String = "")

        If String.IsNullOrEmpty(Skoroszyt) Then
            oBook = sender.ActiveWorkbook
        Else
            oBook = sender.Workbooks(Skoroszyt)
        End If
        If String.IsNullOrEmpty(NazwaArkusza) Then
            oSheet = CType(oBook.ActiveSheet, XL._Worksheet)
        Else
            oSheet = CType(oBook.Worksheets(NazwaArkusza), XL._Worksheet)
        End If
    End Sub

    Function fLastRow(ByVal sender As Object, _
                      Optional ByVal Arkusz As String = "", _
                      Optional ByVal StartRow As Integer = 1) As Integer

        Dim i, j As Integer
        oApp = CType(sender, XL._Application)
        AktywnyArkusz(sender, Arkusz)
        With oSheet
            i = .UsedRange.Row - 1 + .UsedRange.Rows.Count
            j = i
            Do Until Not oApp.WorksheetFunction.CountA(.Rows(j)).Equals(0)
                j -= 1
                If j.Equals(0) Then Exit Do
            Loop
            i = j
            If StartRow > 0 Then _
              If i < StartRow Then i = StartRow
        End With
        Return i
    End Function

    Function fLastAddress(ByVal sender As Object, _
                          Optional ByVal Arkusz As String = "", _
                          Optional ByRef Wiersz As Integer = 1, _
                          Optional ByRef Kolumna As Integer = 1, _
                          Optional ByVal StartRow As Integer = 1) As String

        Dim iRow, iR, iCol, iC As Integer
        oApp = CType(sender, XL._Application)
        AktywnyArkusz(oApp, Arkusz)
        With oSheet
            Try
                iRow = .UsedRange.Row - 1 + .UsedRange.Rows.Count
                iCol = .UsedRange.Column - 1 + .UsedRange.Columns.Count
                iR = iRow
                Do Until Not oApp.WorksheetFunction.CountA(.Rows(iR)).Equals(0)
                    iR -= 1
                Loop
                Wiersz = iR
                iC = iCol
                Do Until Not oApp.WorksheetFunction.CountA(.Columns(iC)).Equals(0)
                    iC -= 1
                Loop
                Kolumna = iC
                If Not Wiersz.Equals(0) AndAlso Not Kolumna.Equals(0) Then
                    If StartRow > 0 Then _
                      If Wiersz < StartRow Then Wiersz = StartRow
                    Return .Cells(Wiersz, Kolumna).Address
                Else
                    Return ""
                End If
            Catch ex As Exception
                Return ""
            Finally
                oSheet = Nothing
            End Try
        End With
    End Function

    Function fLastColumn(ByVal sender As Object, _
                         Optional ByVal Arkusz As String = "", _
                         Optional ByVal StartCol As Integer = 1) As Integer

        Dim i, j As Integer
        AktywnyArkusz(sender, Arkusz)
        With oSheet
            i = .UsedRange.Column - 1 + .UsedRange.Columns.Count
            j = i
            Do Until Not oApp.WorksheetFunction.CountA(.Columns(j)).Equals(0)
                j -= 1
                If j.Equals(0) Then Exit Do
            Loop
            i = j
            If StartCol > 0 Then _
              If i < StartCol Then i = StartCol
        End With
        oSheet = Nothing
        Return i
    End Function

    Function fNewRow(ByVal sender As Object, _
                     ByVal Start As String, _
                     Optional ByVal Arkusz As String = "") As Integer

        Dim iC As Integer
        AktywnyArkusz(sender, Arkusz)
        With oSheet
            iC = .Range(Start).Column
            fNewRow = .Cells(.Cells.Rows.Count, iC).End(oApp.xlUp).Row + 1
        End With
        oSheet = Nothing
    End Function

    Function fSheetExist(ByVal sender As Object, _
                         ByVal Nazwa As String, _
                         Optional ByVal Usun As Boolean = False, _
                         Optional ByVal Dodaj As Boolean = False) As Boolean

        Try
            oApp = CType(sender, XL._Application)
            oBook = oApp.ActiveWorkbook
            oSheet = CType(oBook.Sheets(Nazwa), XL.Worksheet)
            Try
                m_Usun = Usun
                m_Dodaj = Dodaj
                If m_Usun Then sender.ActiveWindow.SelectedSheets.Delete()
                If m_Dodaj Then
                    With oBook
                        .Sheets.Add(Before:=.Worksheets(1))
                        .ActiveSheet.name = Nazwa
                    End With
                End If
            Catch ex As Exception
                Using New CM
                    MessageBox.Show(ex.Message, Nazwa, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
            End Try
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Sub pAddRow2Range(ByRef Obszar As XL.Range)

        With Obszar
            With .Rows(.Rows.Count)
                .EntireRow.Copy()
                .Insert(Shift:=oApp.xlDown)
            End With
        End With
    End Sub

    ' Usuwanie/wyświetlanie linków
    Private Sub LinksRemove()

        pBreakLinks()
    End Sub

    Private Sub LinksShow()

        pBreakLinks(DoArkusza:="Tmp")
    End Sub

    Sub pBreakLinks(Optional ByVal Pokaz As Boolean = True, _
                    Optional ByVal DoArkusza As String = "", _
                    Optional ByVal Skoroszyt As XL._Workbook = Nothing)

        Dim aLinks As Object
        Dim strLink As Object
        Dim blnJestArkusz As Boolean
        Try
            blnJestArkusz = DoArkusza <> ""
            If Skoroszyt Is Nothing Then _
                  Skoroszyt = oApp.ThisWorkbook
            If blnJestArkusz Then
                oSheet = Skoroszyt.Sheets(DoArkusza)
                oSheet.Cells.ClearContents()
            End If
            aLinks = oApp.ActiveWorkbook.LinkSources(Type:=oApp.xlLinkTypeExcelLinks)
            If aLinks Is Nothing Then Exit Try
            pScreenRefresh(False, "Sprawdzam kolekcję łączy...")
            For Each strLink In aLinks
                If blnJestArkusz Then
                    oSheet.Rows(1).Insert(Shift:=oApp.xlUp)
                    oSheet.Cells(1, 1) = strLink
                Else
                    If Pokaz Then
                        Using New CM
                            If MessageBox.Show(String.Concat(strLink, vbCrLf, "Usunąć?"), "Usuwanie łączy", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(vbNo) _
                                Then GoTo Odpusc
                        End Using
                    End If
                    oApp.ActiveWorkbook.BreakLink(Name:=strLink, _
                            Type:=oApp.xlLinkTypeExcelLinks)
                End If
Odpusc:
            Next strLink
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Edycja łączy", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        End Try
    End Sub

    Sub pClearRange(ByVal Adres As String, _
                    Optional ByVal Arkusz As String = "", _
                    Optional ByVal Zeszyt As String = "")

        Try
            AktywnyArkusz(Arkusz, Zeszyt)
            oSheet.Range(Adres). _
                  SpecialCells(oApp.xlCellTypeConstants).ClearContents()
        Catch ex As Exception
        End Try
    End Sub

    Sub pFilterStore(ByVal Arkusz As String, _
                     ByRef aFilter(,) As Object, _
                     ByRef strFilter As String)

        Dim iC As Integer
        AktywnyArkusz(Arkusz)
        strFilter = ""
        If Not oSheet.AutoFilterMode Then GoTo Koniec
        With oSheet.AutoFilter
            strFilter = .Range.Address
            With .Filters
                ReDim aFilter(0 To .Count - 1, 0 To 2)
                For iC = 1 To .Count
                    With .Item(iC)
                        If .On Then
                            aFilter(iC - 1, 0) = .Criteria1
                            If .Operator Then _
                              aFilter(iC - 1, 1) = .Operator : aFilter(iC - 1, 2) = .Criteria2
                        End If
                    End With
                Next iC
            End With
        End With
        oSheet.AutoFilterMode = False
Koniec:
        oSheet = Nothing
    End Sub

    Sub pFilterRestore(ByVal Arkusz As String, _
                       ByRef aFilter(,) As Object, _
                       ByRef strFilter As String)

        Dim iC As Integer
        If String.IsNullOrEmpty(strFilter) Then Exit Sub
        AktywnyArkusz(Arkusz)
        With oSheet
            .AutoFilterMode = False
            .Range(strFilter).AutoFilter()
            For iC = 0 To aFilter.GetUpperBound(0)
                If Not aFilter(iC, 0) Is Nothing Then
                    If Not aFilter(iC, 2) Is Nothing Then
                        .Range(strFilter).AutoFilter(Field:=iC + 1, _
                                Criteria1:=aFilter(iC, 0), _
                                Operator:=aFilter(iC, 1), _
                                Criteria2:=aFilter(iC, 2))
                    Else
                        .Range(strFilter).AutoFilter(Field:=iC + 1, _
                                Criteria1:=aFilter(iC, 0))
                    End If
                End If
            Next iC
        End With
        oSheet = Nothing
    End Sub

    Sub pFindSettings(Optional ByVal Restore As Boolean = False)

        Dim rng As XL.Range
        Dim rg1 As XL.Range
        Dim fs As XL.Range
        Const strKey As String = "krzyś"
        Static aParam(0 To 3) As Object
        Try
            oBook = oApp.ThisWorkbook
            If Restore Then GoTo Finish
            pScreenRefresh(Odswiez:=False)
            oSheet = oBook.Worksheets.Add()
            rng = oSheet.Range("A1:A2")
            rg1 = rng(1, 1)
            ' Określenie komentarz/formuła/wartość
            rg1.AddComment(strKey)
            fs = rng.Find(What:=strKey)
            If Not fs Is Nothing Then
                aParam(1) = oApp.xlComments
            Else
                With rg1
                    .Comment.Delete()
                    .Value = strKey
                    .EntireRow.Hidden = True
                End With
                fs = rng.Find(What:=strKey)
                If Not fs Is Nothing Then
                    aParam(1) = oApp.xlFormulas
                Else
                    aParam(1) = oApp.xlValues
                End If
            End If
            ' Określenie część/całość
            With rg1
                .Value = strKey
                .EntireRow.Hidden = False
            End With
            fs = rng.Find(What:=Right(strKey, 1), LookIn:=oApp.xlValues)
            If fs Is Nothing Then
                aParam(2) = oApp.xlWhole
            Else
                aParam(2) = oApp.xlPart
            End If
            ' Określenie wiersz/kolumna
            With rng
                rng = .Resize(, 2)
                .ClearContents()
                .Cells(1, 2) = strKey
                .Cells(2, 1) = strKey
            End With
            fs = rng.Find(What:=strKey, LookIn:=oApp.xlValues)
            If fs.Address.Equals(rng(1, 2).Address) Then
                aParam(3) = oApp.xlByRows
            Else
                aParam(3) = oApp.xlByColumns
            End If
            ' Określenie rozróżnia małe/wielkie litery
            fs = rng.Find(What:=strKey.ToUpper, LookIn:=oApp.xlValues, SearchOrder:=aParam(2))
            aParam(3) = fs Is Nothing
            oSheet.Delete()
            oSheet = Nothing
            rng = Nothing
            rg1 = Nothing
            pScreenRefresh(True)
Finish:
            fs = oBook.ActiveSheet.Cells.Find(What:="", _
                                              LookIn:=aParam(0), _
                                              LookAt:=aParam(1), _
                                              SearchOrder:=aParam(2), _
                                              MatchCase:=aParam(3))
            fs = Nothing
        Catch ex As Exception
        End Try
    End Sub

    ' Rozciąga kolor na sąsiednie komórki w wierszu w zależności od długości tekstu
    Sub pFormatRozwojowe(ByVal Obszar As XL.Range, _
                         Optional ByVal Kolor As Integer = 0)

        Dim iC As Integer, _
            iK As Integer, _
            strFormula As String
        If Kolor.Equals(0) Then Kolor = 15 ' szary 25%
        For iC = 1 To Obszar.Columns.Count
            With Obszar(1, iC)
                .FormatConditions.Delete()
                If iC.Equals(1) Then
                    strFormula = "=DŁ(WK)>0"
                Else
                    strFormula = "=DŁ(PRZESUNIĘCIE(WK;0;-" & iC - 1 & _
                                 "))>KOMÓRKA(""width"";PRZESUNIĘCIE(WK;0;-" & iC - 1 & "))"
                    If iC > 2 Then
                        For iK = iC - 2 To 1 Step -1
                            strFormula = strFormula & "+KOMÓRKA(""width"";PRZESUNIĘCIE(WK;0;-" & iK & "))"
                        Next iK
                    End If
                End If
                .FormatConditions.Add(Type:=oApp.xlExpression, Formula1:=strFormula)
                .FormatConditions(1).Interior.ColorIndex = Kolor
            End With
        Next iC
    End Sub

    ' Wyświetla/usuwa nazwy
    Private Sub NamesShow()

        pHiddenNames(False, "Tmp")
    End Sub

    Private Sub NamesRemove()

        pHiddenNames(True, "Tmp")
    End Sub

    Sub pHiddenNames(Optional ByVal Usun As Boolean = False, _
                     Optional ByVal NazwaArkusza As String = "")

        Dim obName As XL.Name
        Dim fs, rng As XL.Range
        Dim blnJestArkusz As Boolean
        Try
            blnJestArkusz = NazwaArkusza <> ""
            If blnJestArkusz Then
                AktywnyArkusz(NazwaArkusza)
                oBook.Worksheets(NazwaArkusza).Select()
                If Usun Then GoTo UsunZListy
                oBook.ActiveSheet.Cells.ClearContents()
            End If
            pScreenRefresh(False, "Sprawdzam kolekcję nazw...")
            For Each obName In oApp.ActiveWorkbook.Names
                If Not obName.Visible Then _
                  obName.Visible = True
                If blnJestArkusz Then
                    With oBook.ActiveSheet
                        .Rows(1).Insert(Shift:=oApp.xlUp)
                        .Cells(1, 1) = "x"
                        .Cells(1, 2) = obName.Name
                        .Cells(1, 3) = "'" & .RefersToR1C1
                    End With
                Else
                    If Usun Then obName.Delete()
                End If
            Next obName
            Exit Try
UsunZListy:
            Using New CM
                If MessageBox.Show("Masz absolutną pewność, że chcesz usunąć zaznaczone nazwy?", oApp.ThisWorkbook.Name, _
                           MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(DialogResult.No) Then Exit Try
            End Using
            pScreenRefresh(False, "Usuwam zaznaczone nazwy...")
            rng = oBook.ActiveSheet.Range("A1").CurrentRegion
            For Each obName In oBook.Names
                If Not obName.Visible Then obName.Visible = True
                fs = oBook.ActiveSheet.Columns(2).Find(obName.Name, LookAt:=oApp.xlWhole)
                If Not fs Is Nothing Then _
                  If oBook.ActiveSheet.Cells(fs.Row, 1).ToString.ToUpper.Equals("X") Then obName.Delete()
            Next obName
            oBook.ActiveSheet.Cells.ClearContents()
            rng = Nothing
            fs = Nothing
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, oApp.ThisWorkbook.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            obName = Nothing
            pScreenRefresh(True)
        End Try
    End Sub

    Sub pPodkreslBlad(ByVal Obszar As XL.Range)

        With Obszar
            .Interior.ColorIndex = 3
            .Font.ColorIndex = 2
            .Font.Bold = True
        End With
    End Sub

    ' Wyświetla/zmienia formuły z zaznaczonego obszaru aktywnego arkusza/całego skoroszytu
    Private Sub FormulasShow()

        pScreenRefresh(False)
        pPokazFormuly(TargetWS:="Tmp", NotRC:=True, _
                      TargetWB:=oApp.ThisWorkbook.Name, _
                      SourceWB:=oApp.ActiveWorkbook.Name)
        pScreenRefresh(True)
    End Sub

    Private Sub FormulasChange()

        Using New CM
            If MessageBox.Show(String.Concat("Polega na wstawieniu w miejsce istniejącej treści z kol. 4;", vbLf, _
                      "jeśli kol. 4 jest pusta, formuła jest usuwana. Kontynuujesz?"), "Automatyczna zmiana formuł", _
                      MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(DialogResult.No) Then Exit Sub
        End Using
        pScreenRefresh(False)
        pPokazFormuly(TargetWS:="Tmp", Zamien:=True, NotRC:=True, _
                      TargetWB:=oApp.ThisWorkbook.Name, SourceWB:=oApp.ActiveWorkbook.Name)
        pScreenRefresh(True)
    End Sub

    Sub pPokazFormuly(ByVal TargetWS As String, _
                      Optional ByVal Zamien As Boolean = False, _
                      Optional ByVal Zakres As String = "", _
                      Optional ByVal NotRC As Boolean = False, _
                      Optional ByVal TargetWB As String = "", _
                      Optional ByVal SourceWB As String = "", _
                      Optional ByVal SourceWS As String = "")

        Dim ac, rng, rgS As XL.Range
        Dim lng As Integer
        Dim ws, wsA As XL._Worksheet
        Dim strAddr As String
        Dim aPoz(,) As Object
        If String.IsNullOrEmpty(SourceWB) Then SourceWB = oApp.ActiveWorkbook.Name
        If String.IsNullOrEmpty(TargetWB) Then TargetWB = oApp.ThisWorkbook.Name
        ws = oApp.Workbooks(TargetWB).Sheets(TargetWS)
        If Not Zamien Then GoTo Pokaz
        rng = ws.Range("A1").CurrentRegion
        aPoz = ws.Range(rng(1, 1), rng(rng.Rows.Count, 4))
        For lng = LBound(aPoz) To UBound(aPoz)
            wsA = oApp.Workbooks(SourceWB).Sheets(aPoz(lng, 0))
            If String.IsNullOrEmpty(aPoz(lng, 3)) Then wsA.Range(aPoz(lng, 1)).Value = "" : GoTo Nastepny
            If NotRC Then
                wsA.Range(rng(lng, 2)).Formula = aPoz(lng, 3)
            Else
                wsA.Range(rng(lng, 2)).FormulaR1C1 = aPoz(lng, 3)
            End If
Nastepny:
        Next lng
        GoTo Finish
Pokaz:
        On Error Resume Next
        ws.Cells.Clear()
        If String.IsNullOrEmpty(SourceWS) Then
            If String.IsNullOrEmpty(Zakres) Then GoTo WszystkieArkusze
            SourceWS = oApp.Workbooks(SourceWB).ActiveSheet.Name
        End If
        If String.IsNullOrEmpty(Zakres) Then
            rgS = oApp.Workbooks(SourceWB).Sheets(SourceWS).UsedRange
        Else
            rgS = oApp.Workbooks(SourceWB).Sheets(SourceWS).Range(Zakres)
        End If
        For Each ac In rgS.SpecialCells(oApp.xlCellTypeFormulas)
            strAddr = ac.Address
            If String.IsNullOrEmpty(strAddr) Then GoTo Nastepny1
            lng += 1
            ws.Cells(lng, 1) = SourceWS
            ws.Cells(lng, 2) = strAddr
            If NotRC Then
                ws.Cells(lng, 3) = "'" & ac.Formula
            Else
                ws.Cells(lng, 3) = "'" & ac.FormulaR1C1
            End If
            strAddr = ""
Nastepny1:
        Next ac
        GoTo Finish
WszystkieArkusze:
        For Each wsA In oApp.Workbooks(SourceWB).Worksheets
            For Each ac In wsA.Cells.SpecialCells(oApp.xlCellTypeFormulas)
                strAddr = ac.Address
                If String.IsNullOrEmpty(strAddr) Then GoTo Nastepny2
                lng += 1
                ws.Cells(lng, 1) = wsA.Name
                ws.Cells(lng, 2) = strAddr
                If NotRC Then
                    ws.Cells(lng, 3) = "'" & ac.Formula
                Else
                    ws.Cells(lng, 3) = "'" & ac.FormulaR1C1
                End If
                strAddr = ""
Nastepny2:
            Next ac
        Next wsA
Finish:
        ac = Nothing
        rng = Nothing
        rgS = Nothing
        ws = Nothing
        wsA = Nothing
        aPoz = Nothing
    End Sub

    Sub pPreviewPage(ByVal sender As Object, _
                     Optional ByVal Pionowa As Boolean = False)

        oApp = CType(sender, XL._Application)
        Dim strAdr, strAc As String
        Dim lng, iC As Integer
        With oApp.ActiveWindow
            .View = oApp.xlPageBreakPreview
            .DisplayZeros = False
        End With
        AktywnyArkusz(oApp)
        strAc = oApp.ActiveCell.Address()
        With oSheet
            If Pionowa Then
                .PageSetup.Orientation = oApp.xlPortrait
            Else
                .PageSetup.Orientation = oApp.xlLandscape
            End If
            strAdr = fLastAddress(sender, Wiersz:=lng, Kolumna:=iC)
            .Columns(iC + 1).ColumnWidth = 3
            .Rows(lng + 1).RowHeight = 10
            strAdr = .Cells(lng + 1, iC + 1).Address
            .Range(.Cells(1, 1), .Cells(lng + 1, iC + 1)).Select()
            .PageSetup.PrintArea = ""
            Try
                If oApp.ReferenceStyle.Equals(oApp.xlR1C1) Then
                    .PageSetup.PrintArea = "R1C1:R" & lng + 1 & "C" & iC + 1
                Else
                    .PageSetup.PrintArea = "A1:" & strAdr
                End If
                .VPageBreaks(1).DragOff(Direction:=oApp.xlToRight, RegionIndex:=1)
            Catch ex As Exception
            End Try
        End With
        oApp.Range(strAc).Select()
    End Sub

    Sub pRangeDelete(ByVal sender As Object, _
                     Optional ByVal Start As String = "", _
                     Optional ByVal Arkusz As String = "")

        oApp = CType(sender, XL._Application)
        Dim lngFirstRow As Integer
        Dim strAddress As String
        Dim rng As XL.Range
        AktywnyArkusz(oApp, Arkusz)
        If String.IsNullOrEmpty(Start) Then Start = "A1"
        With oSheet
            Try
                .AutoFilterMode = False
                lngFirstRow = .Range(Start).Row
                strAddress = fLastAddress(sender, Arkusz:=Arkusz, StartRow:=lngFirstRow)
                rng = .Range(Start & ":" & strAddress)
                rng.EntireRow.Delete()
            Catch ex As Exception
                Using New CM
                    MessageBox.Show(ex.Message, "Błąd czyszczenia obszaru", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            End Try
        End With
        rng = Nothing
    End Sub

    ' Zwalnianie zasobów
    Sub pReleaseObject(ByVal obj As Object)

        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Sub pScreenRefresh(ByVal Odswiez As Boolean, _
                       Optional ByVal Komunikat As String = "", _
                       Optional ByVal oXl As XL.Application = Nothing)

        Static intCalculation As Integer
        Try
            If oXl Is Nothing Then oXl = oApp
            With oXl
                If Odswiez Then
                    .DisplayAlerts = True
                    .Calculation = intCalculation
                    .ScreenUpdating = True
                    .StatusBar = False
                    screenFresh = True
                Else
                    Try
                        intCalculation = .Calculation
                    Catch ex As Exception
                        intCalculation = XL.XlCalculation.xlCalculationAutomatic
                    End Try
                    .DisplayAlerts = False
                    .Calculation = XL.XlCalculation.xlCalculationManual
                    .ScreenUpdating = False
                    If String.IsNullOrEmpty(Komunikat) Then
                        .StatusBar = "Przetwarzanie, czekaj proszę..."
                    Else
                        .StatusBar = Komunikat
                    End If
                    screenFresh = False
                End If
            End With
        Catch ex As Exception
        End Try
    End Sub

    ' Wybór pliku Excela
    Function fWybierzPlik(ByVal Szablon As String) As String

        Dim ofd As New OpenFileDialog
        Dim di As New DirectoryInfo(Szablon)
        Try
            With ofd
                .InitialDirectory = di.FullName
                .Filter = "Pliki Excel (*.xl*)|*.xl*"
                .Multiselect = False
                .ReadOnlyChecked = True
                If .ShowDialog.Equals(Windows.Forms.DialogResult.OK) Then _
                    Return .FileName
            End With
        Catch ex As Exception
        End Try
        Return ""
    End Function

    Private Sub XlsClean()

        pReleaseObject(oSheet)
        Try
            oBook.Close()
        Catch ex As Exception
        End Try
        pReleaseObject(oBook)
        Try
            oApp.Quit()
        Catch ex As Exception
        End Try
        pReleaseObject(oApp)
    End Sub

End Module
