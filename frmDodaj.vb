﻿
Imports System.ComponentModel
Imports System.Data
Imports System.Data.OleDb
Imports System.Windows.Forms

Public Class frmDodaj

    Private DlaZakladu As String
    Private MF As frmMain

    Public Property Zaklad() As String

        Get
            Return DlaZakladu
        End Get

        Set(ByVal value As String)
            DlaZakladu = value
        End Set

    End Property

    Private Sub frmDodaj_Load() Handles Me.Load
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable
        Dim i As Integer

        MF = frmMain

        With Me.cmbZaklad
            .DataSource = Nothing
            If DlaZakladu = "1" Then
                da = New OleDbDataAdapter("SELECT DISTINCT Zaklad, ZakladOpis FROM tblSklady ORDER BY ZakladOpis", MF.cns)
                da.Fill(dt)
                .DataSource = dt
                .DisplayMember = "ZakladOpis"
                .ValueMember = "Zaklad"
                .Enabled = True
                .Text = "Drapol"
                da = New OleDbDataAdapter("SELECT TOP 1 * FROM tblKumulacja", MF.cns)
            Else
                .Enabled = False
                da = New OleDbDataAdapter("SELECT TOP 1 * FROM tblEstLit", MF.cns)
            End If
        End With

        dt = New DataTable
        da.Fill(dt)

        Me.cmbPole.DataSource = Nothing

        For i = 3 To dt.Columns.Count - 1
            Me.cmbPole.Items.Add(dt.Columns(i).Caption)
        Next i

        Zaklad_Click()

        If DlaZakladu = "1" Then
            With Me
                .cmbPole.Text = "WDR1"
                .cmbSklad.Text = "HE14 PIŁA - PLAC"
            End With
        End If

        Me.txtKwota.Focus()

    End Sub

    Private Sub Dodaj() Handles OK_Button.Click
        Dim m_Kwota As Double

        Try
            With Me
                If .cmbSklad.SelectedIndex < 0 Then _
                    Throw New ArgumentException("Określ skład / ITEM")
                If .cmbPole.SelectedIndex < 0 Then _
                    Throw New ArgumentException("Wybierz pole do zmiany")
                Try
                    m_Kwota = CDbl(.txtKwota.Text)
                    If m_Kwota = 0 Then Throw New ArgumentException
                Catch ex As Exception
                    Throw New ArgumentException("wpisz prawidłowo wartość/Wagę")
                End Try
            End With

            Dim tr As OleDbTransaction = Nothing
            Dim cmd As New OleDbCommand

            Using cn As New OleDbConnection(MF.cns.ConnectionString)

                Try

                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                    End With

                    With cmd
                        If DlaZakladu = "1" Then
                            .CommandText = "UPDATE tblKumulacja SET " + Me.cmbPole.Text + " = " + Me.cmbPole.Text + " + " + m_Kwota.ToString.Replace(",", ".") _
                                            + " WHERE Zaklad = '" + Me.cmbZaklad.SelectedValue + "' AND Sklad = '" + Me.cmbSklad.SelectedValue + "' AND Wejscie = #" + MF.dtpOkres.Value.ToShortDateString + "#"
                        Else
                            .CommandText = "UPDATE tblEstLit SET " + Me.cmbPole.Text + " = " + Me.cmbPole.Text + " + " + m_Kwota.ToString.Replace(",", ".") _
                                            + " WHERE Zaklad = 'LTS' AND ITEM = '" + Me.cmbSklad.SelectedValue + "' AND END_DATE = #" + MF.dtpOkres.Value.ToShortDateString + "#"
                        End If
                        .ExecuteNonQuery()
                    End With

                    tr.Commit()

                    Me.txtKwota.Clear()

                Catch ex As Exception

                    tr.Rollback()
                    MessageBox.Show(ex.Message + vbCrLf + ex.StackTrace, "Dodaj do pola", MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try

            End Using

            tr = Nothing
            cmd = Nothing

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Dodaj do pola", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub

    Private Sub Zaklad_Click() Handles cmbZaklad.SelectionChangeCommitted
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable

        With Me.cmbSklad
            .DataSource = Nothing
            If Me.cmbZaklad.SelectedIndex > -1 Then
                da = New OleDbDataAdapter("SELECT Sklad, Sklad + ' ' + SkladOpis AS [Opis] FROM tblSklady WHERE Zaklad = '" + Me.cmbZaklad.SelectedValue + "' ORDER BY SkladOpis", MF.cns)
                da.Fill(dt)
                .DataSource = dt
                .DisplayMember = "Opis"
                .ValueMember = "Sklad"
            ElseIf DlaZakladu = "2" Then
                da = New OleDbDataAdapter("SELECT ITEM FROM tblEstLit WHERE Zaklad = 'LTS' AND END_DATE = #" + MF.dtpOkres.Value.ToShortDateString + "# ORDER BY ITEM", MF.cns)
                da.Fill(dt)
                .DataSource = dt
                .DisplayMember = "ITEM"
                .ValueMember = "ITEM"
            End If
        End With
    End Sub

End Class
