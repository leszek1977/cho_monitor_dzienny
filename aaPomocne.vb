﻿
#Region "Opcje i referencje"

Option Explicit On
Option Strict Off
Option Compare Text
Option Infer On

' Pomocne ogólnego zastosowania
Imports System.IO
Imports System.Data.OleDb
Imports CM = CCenterMsg.CenterMsg

#End Region ' Opcje i referencje

' Modified 2014-08-12
Module aaPomocne

    Public Enum Stan
        Kopia = 4
        Niewybrany = -1
        Nowy = 1
        Skresl = 3
        Wybrany = 0
        Zmiana = 2
    End Enum

#Region "Konwersja tekstu rozdzielanego |,|| na tabelę i odwrotnie"

    Function ConvertDt2Pos(ByVal Tabela As DataTable) As String

        Dim i As Integer
        Dim m_Ret As String = ""
        Try
            If Tabela Is Nothing Then Exit Try
            If Tabela.Rows.Count.Equals(0) Then Exit Try
            For Each r As DataRow In Tabela.Rows
                For i = 0 To r.ItemArray.GetUpperBound(0)
                    Select Case Tabela.Columns(i).DataType.Name
                        Case "Boolean"
                            If Not IsDBNull(r(i)) AndAlso CBool(r(i)) Then
                                m_Ret += "T|"
                            Else
                                m_Ret += "N|"
                            End If
                        Case "Double"
                            Try
                                m_Ret += r(i).ToString.Replace(",", ".") + "|"
                            Catch ex As Exception
                                m_Ret += "0|"
                            End Try
                        Case "Int16", "Int32", "Int64"
                            Try
                                m_Ret += r(i).ToString + "|"
                            Catch ex As Exception
                                m_Ret += "0|"
                            End Try
                        Case Else
                            m_Ret += r(i).ToString + "|"
                    End Select
                Next i
                m_Ret += "|"
            Next r
            With m_Ret
                If .EndsWith("||") Then m_Ret = .Substring(0, .Length - 2)
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Konwersja tabeli na tekst", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        End Try
        Return m_Ret
    End Function

    Function ConvertDt2Pos(ByVal Tabela As DataTable, _
                           ByVal IleKolum As Integer, _
                           ByVal NrKolumny As Integer, _
                           ByRef Wartosc As Double, _
                           Optional ByVal Warunek As Integer = -1) As String

        Dim i As Integer
        Dim m_Ret As String = ""
        Dim dbl As Double
        Try
            Wartosc = 0
            If Tabela Is Nothing Then Exit Try
            If Tabela.Rows.Count.Equals(0) Then Exit Try
            For Each r As DataRow In Tabela.Rows
                For i = 0 To IleKolum
                    Select Case Tabela.Columns(i).DataType.Name
                        Case "Boolean"
                            If Not IsDBNull(r(i)) AndAlso CBool(r(i)) Then
                                m_Ret += "T|"
                            Else
                                m_Ret += "N|"
                            End If
                        Case "Double"
                            Try
                                m_Ret += r(i).ToString.Replace(",", ".") + "|"
                            Catch ex As Exception
                                m_Ret += "0|"
                            End Try
                        Case "Int16", "Int32", "Int64"
                            Try
                                m_Ret += r(i).ToString + "|"
                            Catch ex As Exception
                                m_Ret += "0|"
                            End Try
                        Case Else
                            m_Ret += r(i).ToString + "|"
                    End Select
                Next i
                Try
                    dbl = CDbl(r(NrKolumny).ToString)
                    If Not Warunek.Equals(-1) Then
                        If CBool(r(Warunek)) Then Wartosc += dbl
                    Else
                        Wartosc += CDbl(r(NrKolumny).ToString)
                    End If
                Catch ex As Exception
                    Throw New ArgumentException(ex.Message)
                End Try
                m_Ret += "|"
            Next r
            With m_Ret
                If .EndsWith("||") Then m_Ret = .Substring(0, .Length - 2)
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Konwersja tabeli na tekst", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        End Try
        Return m_Ret
    End Function

    Sub ConvertPos2Dt(ByVal Tekst As String, _
                      ByRef Tabela As DataTable)

        Dim i, j As Integer
        Dim aPoz() As String
        Dim aItem() As String
        Dim r As DataRow = Nothing
        Try
            Tabela.Clear()
            aPoz = Split(Tekst.ToString, "||")
            i = aPoz.Length
            If i.Equals(0) OrElse String.IsNullOrEmpty(aPoz(0)) Then Exit Try
            For i = 0 To aPoz.GetUpperBound(0)
                If String.IsNullOrEmpty(aPoz(i)) Then Exit For
                aItem = Split(aPoz(i).ToString, "|")
                r = Tabela.NewRow
                For j = 0 To aItem.GetUpperBound(0)
                    Select Case Tabela.Columns(j).DataType.Name
                        Case "Boolean"
                            r(j) = True
                            If aItem(j).ToUpper.Equals("FALSE") _
                                OrElse aItem(j).ToUpper.Equals("FAŁSZ") _
                                OrElse aItem(j).ToUpper.Equals("N") _
                                    Then r(j) = False
                        Case "Double"
                            Try
                                r(j) = CDbl(aItem(j).ToString.Replace(".", ","))
                            Catch ex As Exception
                                r(j) = 0
                            End Try
                        Case "Single"
                            Try
                                r(j) = CSng(aItem(j).ToString.Replace(".", ","))
                            Catch ex As Exception
                                r(j) = 0
                            End Try
                        Case "Int16", "Int32", "Int64"
                            Try
                                r(j) = CInt(aItem(j))
                            Catch ex As Exception
                                r(j) = 0
                            End Try
                        Case Else
                            r(j) = aItem(j)
                    End Select
                Next j
                Tabela.Rows.Add(r)
            Next i
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Konwersja tekstu na tabelę", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            aPoz = Nothing
            aItem = Nothing
            r = Nothing
        End Try
    End Sub

    Function TextPosConv2Table(ByVal dt As DataTable, _
                               ByVal Kolumna As String, _
                               ByVal dtP As DataTable) As DataTable

        Dim dtF As New DataTable
        Dim r As DataRow
        Dim i As Integer
        Try
            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException
            dtF = dt.Clone
            dtF.Columns.Remove(Kolumna)
            For Each c As DataColumn In dtP.Columns
                dtF.Columns.Add(c.ColumnName, c.DataType)
            Next c
            For Each r1 As DataRow In dt.Rows
                ConvertPos2Dt(r1(Kolumna).ToString, dtP)
                For Each r2 As DataRow In dtP.Rows
                    r = dtF.NewRow
                    For i = 0 To dt.Columns.Count - 1
                        With dt.Columns(i)
                            If Not .ColumnName.Equals(Kolumna) Then
                                r(.ColumnName) = r1(i)
                            End If
                        End With
                    Next i
                    For i = 0 To dtP.Columns.Count - 1
                        r(dtP.Columns(i).ColumnName) = r2(i)
                    Next i
                    dtF.Rows.Add(r)
                Next r2
            Next r1
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Tworzenie tabeli pozycji", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            dtF = Nothing
        End Try
        Return dtF
    End Function

    Sub ConvertPos2Array(ByVal Tekst As String, ByRef aRet(,) As String)

        Dim i, j As Integer
        Dim aPoz() As String
        Dim aItem() As String
        Try
            Tekst = Tekst.Replace("]", "|")
            aPoz = Split(Tekst.ToString, "||")
            i = aPoz.Length
            If i.Equals(0) OrElse String.IsNullOrEmpty(aPoz(0)) Then Exit Try
            aItem = Split(aPoz(0).ToString, "|")
            j = aItem.Length - 1
            ReDim aRet(i - 1, j)
            For i = 0 To aPoz.GetUpperBound(0)
                If String.IsNullOrEmpty(aPoz(i)) Then Exit For
                aItem = Split(aPoz(i).ToString, "|")
                For j = 0 To aItem.GetUpperBound(0)
                    If Not String.IsNullOrEmpty(aItem(j)) Then aRet(i, j) = aItem(j)
                Next j
            Next i
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Konwersja tekstu na tabelę", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            aRet = Nothing
        Finally
            aPoz = Nothing
            aItem = Nothing
        End Try
    End Sub

#End Region 'Konwersja tekstu rozdzielanego |,|| na tabelę/tablicę i odwrotnie

    ' Przechodzenie pomiędzy kontrolkami po naciśnięciu Enter
    Sub EnterKeyPressed(ByVal sender As Object, _
                        ByVal e As System.Windows.Forms.KeyPressEventArgs)

        '   podczas inicjacji wstawić:
        '           AddHandler Ctrl.KeyPress, AddressOf EnterKeyPressed
        Dim Ctrl As Control = CType(sender, Control)
        Dim m_Parent As Control = Ctrl.Parent
        If Not e.KeyChar.Equals(Chr(13)) Then Return
        Try
            If Ctrl.TabIndex.Equals(m_Parent.Controls.Count - 1) Then
                m_Parent.Parent.SelectNextControl(Ctrl, True, True, True, False)
            Else
                m_Parent.SelectNextControl(Ctrl, True, True, True, False)
            End If
        Catch ex As Exception
            Ctrl.Focus()
        End Try
    End Sub

    ' Przypisanie klawisza <Enter> do kontrolek w TableLayoutPanel
    Sub Enter4Tlp(ByVal tlp As TableLayoutPanel)

        For Each Ctrl As Control In tlp.Controls
            If Ctrl.TabStop Then
                With Ctrl.GetType.ToString
                    If .Contains("Text") OrElse .Contains("Combo") OrElse .Contains("Date") Then _
                        AddHandler Ctrl.KeyPress, AddressOf EnterKeyPressed
                End With
            End If
        Next Ctrl
    End Sub

    Function fArrayLayoutControl(ByVal Dane As Array, _
                                 ByVal Kontrolny As String, _
                                 ByVal cns As OleDbConnection) As Boolean

        ' Source (nazwa pliku źródłowego),
        ' NrKol (kolumna pliku źródłowego)
        ' KolPodstawowa (bez spacji i #; nazwy używane przez program)
        ' Wariant1 - alternatywna, kontrolna nazwa
        ' Wariant2 - jw.
        ' Wariant3 - jw.
        Dim i, j, iC As Integer
        Dim dt As New DataTable
        Dim r As DataRow
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT * FROM tblSources WHERE Source = '" + Kontrolny + "' ORDER BY NrKol", cns)
        da.Fill(dt)
        Try
            For i = 1 To Dane.GetLength(1)
                Dane(1, i) = Dane.GetValue(1, i).ToString.Replace(" ", "").Replace("#", "").Replace(".", "").ToUpper
            Next i
            ' porównanie kolumn
            For iC = 0 To dt.Rows.Count - 1
                r = dt.Rows(iC)
                For i = 1 To Dane.GetLength(1)
                    If r("KolPodstawowa").ToString.ToUpper.Equals(Dane.GetValue(1, i).ToString) Then
                        If Not CInt(r("NrKol").ToString).Equals(i) Then
                            Throw New ArgumentException("Zmieniony układ (kolumna [" + r("KolPodstawowa").ToString + "])")
                        Else
                            GoTo Dalej
                        End If
                    Else
                        ' przeszukaj pomocnicze kolumny
                        For j = 1 To 3
                            With r(j + 2)
                                If Not String.IsNullOrEmpty(.ToString) AndAlso .ToString.ToUpper.Equals(Dane.GetValue(1, i).ToString) Then
                                    If Not CInt(r("NrKol").ToString).Equals(i) Then
                                        Throw New ArgumentException("Zmieniony układ (kolumna [" + r(j + 2).ToString + "])")
                                    Else
                                        GoTo Dalej
                                    End If
                                End If
                            End With
                        Next j
                    End If
                Next i
                Throw New ArgumentException("Niezidentyfikowana kolumna [" + r("KolPodstawowa").ToString + "]")
Dalej:
            Next iC
            Return True
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, Kontrolny, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Return False
        Finally
            dt = Nothing
            da = Nothing
            r = Nothing
        End Try
    End Function

    ' Sprawdza poprawność wprowadzonej daty w TextBox
    Function fCheckDate(ByVal sender As TextBox, _
                           Optional ByVal SetFocus As Boolean = False, _
                           Optional ByRef dtmDate As DateTime = Nothing) As Boolean

        Try
            Dim dtm As DateTime = CDate(sender.Text)
            dtmDate = dtm.Date
        Catch ex As Exception
            If SetFocus Then sender.Focus()
            dtmDate = Nothing
            Return False
        End Try
        Return True
    End Function

    ' Sprawdza istnienie folderu i gdy brak tworzy
    Function fCheckFolder(ByVal Nazwa As String) As Boolean

        Dim vSplit() As String
        Dim i As Integer
        Dim m_Path As String
        Dim m_Ret As Boolean = True
        Try
            If My.Computer.FileSystem.DirectoryExists(Nazwa) Then Exit Try
            With Nazwa
                If .EndsWith(Path.DirectorySeparatorChar) Then Nazwa = .Substring(0, .Length - 1)
                vSplit = .Split(Path.DirectorySeparatorChar)
            End With
            m_Path = vSplit(0)
            For i = 1 To UBound(vSplit, 1)
                m_Path += Path.DirectorySeparatorChar + vSplit(i)
                If Not My.Computer.FileSystem.DirectoryExists(m_Path) Then _
                    My.Computer.FileSystem.CreateDirectory(m_Path)
            Next i
        Catch ex As Exception
            m_Ret = False
        End Try
        Return m_Ret
    End Function

    ' Sprawdzenie układu kolumn
    Function fCheckSourceColumns(ByVal Source As DataTable, ByRef Tabela As DataTable) As Boolean

        Dim i, j, k As Integer
        Dim aCol(0 To Source.Rows.Count - 1, 0 To 1) As Integer
        Dim txt As String
        Dim r As DataRow
        Try
            ' porównanie kolumn
            For i = 0 To Source.Rows.Count - 1
                r = Source.Rows(i)
                aCol(i, 0) = CInt(r("NrKol").ToString)
                For j = 0 To Tabela.Columns.Count - 1
                    txt = Tabela.Columns(j).ColumnName.ToString.Trim.ToUpper.Replace(".", "").Replace("#", "").Replace(" ", "")
                    If r("KolPodstawowa").ToString.ToUpper.Replace(".", "").Replace(" ", "").Equals(txt) Then
                        aCol(i, 1) = j + 1
                        Tabela.Columns(j).ColumnName = Source.Rows(i)("KolPodstawowa").ToString
                        GoTo Nastepny
                    Else
                        ' przeszukaj pomocnicze kolumny
                        For k = 1 To 3
                            If r(k + 2).ToString.ToUpper.Replace(".", "").Replace(" ", "").Equals(txt) Then
                                aCol(i, 1) = j + 1
                                Tabela.Columns(j).ColumnName = r("KolPodstawowa").ToString
                                GoTo Nastepny
                            End If
                        Next k
                    End If
                Next j
Nastepny:
            Next i
            ' sprawdzenie, czy wszystkie dobrane
            For i = 0 To Source.Rows.Count - 1
                If aCol(i, 1).Equals(0) Then _
                    Throw New ArgumentException("Położenie kolumny [" + Source.Rows(i)("KolPodstawowa").ToString + "] nie zostało ustalone")
            Next i
            Return True
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Kontrola układu kolumn", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Return False
        End Try
    End Function

    Function fCheckSourceColumns(ByVal Source As DataTable, ByRef Tablica As Object) As Boolean

        ' dodane usuwanie twardej spacji ASC 160
        Dim i, j, k As Integer
        Dim aCol(0 To Source.Rows.Count - 1, 0 To 1) As Integer
        Dim txt As String
        Dim r As DataRow
        Try
            ' porównanie kolumn
            For i = 0 To Source.Rows.Count - 1
                r = Source.Rows(i)
                aCol(i, 0) = CInt(r("NrKol").ToString)
                For j = 1 To Tablica.GetUpperBound(1)
                    txt = Tablica(1, j).Trim.ToUpper.Replace(".", "").Replace("#", "").Replace(" ", "").Replace(Chr(160), "")
                    If r("KolPodstawowa").ToString.ToUpper.Replace(".", "").Replace(" ", "").Replace(Chr(160), "").Equals(txt) Then
                        aCol(i, 1) = j
                        Tablica(1, j) = r("KolPodstawowa").ToString
                        GoTo Nastepny
                    Else
                        ' przeszukaj pomocnicze kolumny
                        For k = 1 To 3
                            If r(k + 2).ToString.ToUpper.Replace(".", "").Replace(" ", "").Replace(Chr(160), "").Equals(txt) Then
                                aCol(i, 1) = j
                                Tablica(1, j) = r("KolPodstawowa").ToString
                                GoTo Nastepny
                            End If
                        Next k
                    End If
                Next j
Nastepny:
            Next i
            ' sprawdzenie, czy wszystkie dobrane
            For i = 0 To Source.Rows.Count - 1
                If aCol(i, 1).Equals(0) Then _
                    Throw New ArgumentException("Położenie kolumny [" + Source.Rows(i)("KolPodstawowa").ToString + "] nie zostało ustalone")
            Next i
            Return True
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Kontrola układu kolumn", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Return False
        End Try
    End Function

    ' Sprawdzanie i uzupełnianie czasu w TextBox
    Function fCheckTime(ByVal sender As TextBox, _
                           Optional ByVal SetFocus As Boolean = False, _
                           Optional ByRef hhTime As DateTime = Nothing) As Boolean

        Dim strHH As String
        Dim iW As Integer
        Try
            strHH = sender.Text.Trim
            If strHH.Length.Equals(0) Then Throw New ArgumentException("")
            iW = strHH.IndexOf(":")
            Select Case iW
                Case -1
                    Select Case strHH.Length
                        Case 1
                            strHH = "0" + strHH + ":00"
                        Case 2
                            strHH += ":00"
                        Case 3
                            strHH = String.Concat(strHH.Substring(0, 2), ":", strHH.Substring(2), "0")
                        Case 4
                            strHH = String.Concat(strHH.Substring(0, 2), ":", strHH.Substring(2))
                        Case Else
                            Throw New ArgumentException("")
                    End Select
                Case 0
                    strHH = "00" + strHH.Substring(0)
                Case 1
                    strHH = "0" + strHH.Substring(0)
                Case 2
                    If strHH.Length.Equals(3) Then
                        strHH += "00"
                    ElseIf strHH.Length.Equals(4) Then
                        strHH += "0"
                    End If
                Case Else
                    Throw New ArgumentException("")
            End Select
            hhTime = CDate(strHH)
        Catch ex As Exception
            If SetFocus Then sender.Focus()
            hhTime = Nothing
            Return False
        End Try
        Return True
    End Function

    ' "ReadOnly" combobox przykrywa/odkrywa nieudostępniony combobox controlką txt
    Sub fComboToText(ByRef txt As TextBox, _
                     ByRef cmb As ComboBox, _
                     Optional ByVal Reverse As Boolean = False)

        Dim m_Location As Point
        Dim m_Size As Size
        Dim m_Anchor As AnchorStyles
        Dim m_RS, m_CS As Integer
        Dim m_Parent As TableLayoutPanel = Nothing
        Dim m_Rodzic As Control
        Dim m_Cel As TableLayoutPanelCellPosition
        Dim m_Idx As Integer = cmb.SelectedIndex
        If Not cmb.Enabled Then Exit Sub
        If Reverse Then
            If cmb.Visible Then GoTo Finish
            Try
                m_Parent = CType(txt.Parent, TableLayoutPanel)
                m_RS = m_Parent.GetRowSpan(txt)
                m_CS = m_Parent.GetColumnSpan(txt)
                m_Cel = m_Parent.GetCellPosition(txt)
            Catch ex As Exception
            End Try
            With txt
                If .Text.Equals(cmb.Text) Then m_Idx = cmb.SelectedIndex
                .Visible = False
                m_Rodzic = .Parent
                m_Location = .Location
                m_Size = .Size
                m_Anchor = .Anchor
                .Parent = cmb.Parent
                .Anchor = cmb.Anchor
                .Size = cmb.Size
                .Location = cmb.Location
            End With
            If Not m_Parent Is Nothing Then
                m_Parent.SetCellPosition(cmb, m_Cel)
                m_Parent.SetRowSpan(cmb, m_RS)
                m_Parent.SetColumnSpan(cmb, m_CS)
            End If
            With cmb
                .Visible = True
                .Parent = m_Rodzic
                .Location = m_Location
                .Size = m_Size
                .Anchor = m_Anchor
                .SelectionLength = 0
                .SelectedIndex = m_Idx
            End With
            GoTo Finish
        End If
        If Not cmb.Visible Then GoTo Finish
        Try
            m_Parent = CType(cmb.Parent, TableLayoutPanel)
            m_RS = m_Parent.GetRowSpan(cmb)
            m_CS = m_Parent.GetColumnSpan(cmb)
            m_Cel = m_Parent.GetCellPosition(cmb)
        Catch ex As Exception
        End Try
        With cmb
            .Visible = False
            m_Rodzic = .Parent
            m_Location = .Location
            m_Size = .Size
            m_Anchor = .Anchor
            .Parent = txt.Parent
            .Anchor = AnchorStyles.None
            .Size = txt.Size
            .Location = txt.Location
            .SelectedIndex = m_Idx
        End With
        If Not m_Parent Is Nothing Then
            m_Parent.SetCellPosition(txt, m_Cel)
            m_Parent.SetRowSpan(txt, m_RS)
            m_Parent.SetColumnSpan(txt, m_CS)
        End If
        With txt
            .Visible = True
            .Parent = m_Rodzic
            .Location = m_Location
            .Size = m_Size
            .Anchor = m_Anchor
        End With
Finish:
        txt.Text = cmb.Text
        m_Parent = Nothing
        m_Rodzic = Nothing
    End Sub

    ' Porównanie daty modyfikacji pliku z zadaną
    Function fCompareFileDate(ByVal FileSpec As String, _
                              ByVal Dtm2Compare As DateTime, _
                              Optional ByVal AreEqual As Boolean = True, _
                              Optional ByVal BeforeDtm2 As Boolean = False, _
                              Optional ByVal LaterOfDtm2 As Boolean = False, _
                              Optional ByVal OnlyDate As Boolean = False) As Boolean

        Try
            Dim fsi As FileInfo = My.Computer.FileSystem.GetFileInfo(FileSpec)
            Dim dtt As DateTime = fsi.LastWriteTime
            Dim result As Integer
            If OnlyDate Then
                result = Dtm2Compare.Date.CompareTo(dtt.Date)
            Else
                result = Dtm2Compare.CompareTo(dtt)
            End If
            If AreEqual Then
                If BeforeDtm2 Then
                    Return result >= 0
                ElseIf LaterOfDtm2 Then
                    Return result <= 0
                Else
                    Return result = 0
                End If
            ElseIf BeforeDtm2 Then
                If LaterOfDtm2 Then
                    Return result <> 0
                Else
                    Return result > 0
                End If
            ElseIf LaterOfDtm2 Then
                Return result < 0
            End If
            Using New CM
                MessageBox.Show("Plik z " + fsi.LastWriteTime.Date + " nie odpowiada warunkom", FileSpec, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Catch ex As Exception
            Return False
        End Try
    End Function

    ' Porównanie dwóch dat (i czasów)
    Function fCompareDates(ByVal Before As Boolean, _
                           ByVal BasicDate As DateTime, _
                           ByVal CompDate As DateTime, _
                           Optional ByVal BasicTime As DateTime = Nothing, _
                           Optional ByVal CompTime As DateTime = Nothing) As Boolean

        Try
            Dim dtm1 As Date = CompDate.Date
            Dim dtm2 As Date = BasicDate.Date
            Dim result_date As Integer = DateTime.Compare(dtm1, dtm2)
            If Before Then
                If result_date < 0 Then Return True
                If result_date.Equals(0) Then
                    Dim result_time As TimeSpan = CompTime - BasicTime
                    Return (result_time.Hours * 60 + result_time.Minutes) < 0
                End If
            Else
                If result_date > 0 Then Return True
                If result_date.Equals(0) Then
                    Dim result_time As TimeSpan = CompTime - BasicTime
                    Return (result_time.Hours * 60 + result_time.Minutes) > 0
                End If
            End If
            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function

    Function fCompareDates(ByVal Before As Boolean, _
                           ByVal BasicDate As TextBox, _
                           ByVal CompDate As TextBox, _
                           Optional ByVal BasicTime As TextBox = Nothing, _
                           Optional ByVal CompTime As TextBox = Nothing) As Boolean

        Try
            Dim dtm1 As Date = CDate(CompDate.Text)
            Dim dtm2 As Date = CDate(BasicDate.Text)
            Dim result_date As Integer = DateTime.Compare(dtm1, dtm2)
            If Before Then
                If result_date < 0 Then Return True
                If result_date.Equals(0) Then
                    Dim result_time As TimeSpan = CDate(CompTime.Text) - CDate(BasicTime.Text)
                    Return (result_time.Hours * 60 + result_time.Minutes) < 0
                End If
            Else
                If result_date > 0 Then Return True
                If result_date.Equals(0) Then
                    Dim result_time As TimeSpan = CDate(CompTime.Text) - CDate(BasicTime.Text)
                    Return (result_time.Hours * 60 + result_time.Minutes) > 0
                End If
            End If
            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function

    ' Konwersja daty z kropką na systemową
    Function fConv2Date(ByVal strDate As String, _
                        Optional ByRef dtmDate As Date = Nothing) As Boolean

        Dim Val As Boolean = False
        Try
            Select Case strDate.IndexOf(".")
                Case 1
                    dtmDate = DateSerial(CInt(strDate.Substring(5, 4)), _
                                         CInt(strDate.Substring(2, 2)), _
                                         CInt(strDate.Substring(0, 1)))
                Case 2
                    dtmDate = DateSerial(CInt(strDate.Substring(6, 4)), _
                                         CInt(strDate.Substring(3, 2)), _
                                         CInt(strDate.Substring(0, 2)))
                Case 4
                    dtmDate = DateSerial(CInt(strDate.Substring(0, 4)), _
                                         CInt(strDate.Substring(3, 2)), _
                                         CInt(strDate.Substring(8, 2)))
                Case Else
                    Throw New ArgumentException
            End Select
            Val = True
        Catch ex As Exception
            Try
                dtmDate = CDate(strDate)
                Val = True
            Catch
            End Try
        End Try
        Return Val
    End Function

    ' Konwersja danych do zapisu w Excel-u
    Function fConv2Str(ByVal Dane As Object) As String

        Try
            Select Case TypeName(Dane)
                Case "String"
                    Return String.Concat("=+", Trim(CType(Dane, String)))
                Case "Date"
                    Return String.Concat("=+", Format(CType(Dane, Date), "yyyy-mm-dd"))
                Case "Integer"
                    Return String.Concat("=+", Trim(CStr(Dane)))
                Case "Long"
                    fConv2Str = String.Concat("=+", Trim(CStr(Dane)))
                Case "Boolean"
                    If CType(Dane, Boolean) Then
                        Return "X"
                    Else
                        Return " "
                    End If
                Case Else
                    Return String.Concat("=+", Trim(CStr(Dane)))
            End Select
        Catch ex As Exception
            Return ""
        End Try
    End Function

    ' Konwersja daty systemowej na datę z kropką
    Function fConvDate2StrDate(ByVal m_Date As String) As String

        Try
            With m_Date
                Return String.Concat(.Substring(8, 2), ".", .Substring(5, 2), ".", .Substring(0, 4))
            End With
        Catch ex As Exception
            Return ""
        End Try
    End Function

    ' Konwersja liczby do zapisu w db (z kropką)
    Function fDbl2Str(ByVal Liczba As Double) As String

        Try
            Return Liczba.ToString.Replace(",", ".")
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    ' Zwraca liczbę dni roboczych w miesiącu
    Function fDniRoboczeWMiesiacu(ByVal Rok As Integer, _
                                  ByVal Miesiac As Integer, _
                                  ByVal Swieta As String, _
                                  Optional ByRef DniRobocze As String = "") As Integer

        Dim iC, iDzien, iDni, iDay As Integer
        Try
            iDzien = DateSerial(Rok, Miesiac + 1, 0).Day
            For iC = 1 To iDzien
                If Not Swieta.Contains(String.Concat(" ", CStr(iC)).Substring(0, 2)) Then
                    iDay = Weekday(DateSerial(Rok, Miesiac, iC), FirstDayOfWeek.Monday)
                    If iDay >= 1 And iDay <= 5 Then
                        DniRobocze += String.Concat("_", Format(DateSerial(Rok, Miesiac, iC).Day, "dd"))
                        iDni += 1
                    End If
                End If
            Next iC
        Catch ex As Exception
            Return 0
        End Try
        Return iDni
    End Function

    ' Wyszukiwanie elementu posortowanej tablicy
    Function fFindRowOfSortTable(ByVal SourceTable(,) As Object, _
                                 ByVal Column2Search As Integer, _
                                 ByVal ToSearch As Object) As Integer

        Dim iMax As Integer = SourceTable.GetUpperBound(0)
        Dim iR As Integer
        Dim iH As Integer = iMax \ 2
        Dim iMin As Integer = 0
        Dim strSearch As String = ToSearch.ToString
        Dim iFound As Integer = -1
        Try
            For iR = 0 To 4
                Select Case SourceTable(iH, Column2Search).ToString
                    Case strSearch
                        iFound = iH
                        Exit Try
                    Case Is > strSearch
                        iMax = iH
                        iH -= (iH - iMin) \ 2
                    Case Else
                        iMin = iH
                        iH += (iMax - iH) \ 2
                End Select
            Next iR
            If SourceTable(iH, Column2Search).ToString > strSearch Then
                For iH = iH To iMin Step -1
                    If SourceTable(iH, Column2Search).ToString.Equals(strSearch) _
                        Then iFound = iH : Exit Try
                Next iH
            Else
                For iH = iH To iMax
                    If SourceTable(iH, Column2Search).ToString.Equals(strSearch) _
                        Then iFound = iH : Exit Try
                Next iH
            End If
        Catch ex As Exception
        End Try
        Return iFound
    End Function

    ' Zwraca pełny tekst angielski daty
    Function fFullEnglishDate(ByVal dm As DateTime, _
                              Optional ByVal BezSufixu As Boolean = False, _
                              Optional ByVal BezDnia As Boolean = False, _
                              Optional ByVal BezRoku As Boolean = False, _
                              Optional ByVal Trzy As Boolean = False) As String

        Dim strSufix As String = ""
        Dim aMonth() As String = {"January", "February", "March", "April", _
                                  "May", "June", "July", "August", _
                                  "September", "October", "November", "December"}
        Dim _Ret As String = ""
        Try
            Dim txt As String = aMonth(dm.Month - 1)
            If Not BezDnia Then
                _Ret = dm.Day.ToString + " "
                If Not BezSufixu Then
                    Select Case dm.Day
                        Case 1
                            strSufix = "st"
                        Case 2
                            strSufix = "nd"
                        Case 3
                            strSufix = "rd"
                        Case Else
                            strSufix = "th"
                    End Select
                    _Ret += strSufix
                End If
            End If
            If Trzy Then
                _Ret += txt.Substring(0, 3)
            Else
                _Ret += txt
            End If
            If Not BezRoku Then _Ret += " " & dm.Year
        Catch ex As Exception
        End Try
        Return _Ret
    End Function

    'Function fFullPolishDate(ByVal dm As Date, _
    '                         Optional ByVal BezDnia As Boolean = False, _
    '                         Optional ByVal BezRoku As Boolean = False, _
    '                         Optional ByVal Trzy As Boolean = False) As String

    '    Dim _Ret As String = ""
    '    Dim txt As String = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dm.Month)
    '    Try
    '        If Not BezDnia Then _Ret = dm.Day.ToString + " "
    '        If Trzy Then
    '            _Ret += txt.Substring(0, 3)
    '        Else
    '            _Ret += txt
    '        End If
    '        If Not BezRoku Then _Ret += " " & dm.Year
    '    Catch ex As Exception
    '    End Try
    '    Return _Ret
    'End Function

    ' Zwraca datę ostatniej modyfikacji pliku
    Function fGetFileDateModif(ByVal FileSpec As String) As DateTime

        Dim fsi As FileInfo = My.Computer.FileSystem.GetFileInfo(FileSpec)
        Return fsi.LastWriteTime.Date
    End Function

    ' Zwraca numer nadanego autoincrement ID
    Function fGetLastID(ByVal Polacznie As OleDbConnection, _
                        ByVal Tabela As String, _
                        ByVal Id As String) As Integer

        Dim cmd As OleDbCommand = New OleDbCommand("SELECT TOP 1 * FROM " + Tabela + " ORDER BY " + Id + " DESC", Polacznie)
        Dim value As Integer = -1
        With cmd
            .Connection.Open()
            value = CInt(.ExecuteScalar.ToString)
            .Connection.Close()
        End With
        cmd = Nothing
        Return value
    End Function

    ' Zwraca datę z nazwy pliku
    Function fGetYearMonth(ByVal strFile As String, _
                           Optional ByVal strSeparator As String = " ", _
                           Optional ByVal blnFullDate As Boolean = False, _
                           Optional ByVal intRokInSplit As Integer = 0) As String

        Dim X() As String
        Dim iC As Integer
        Dim dtm As DateTime
        Dim _Ret As String = ""
        Try
            X = Split(strFile, strSeparator)
            If intRokInSplit > 0 Then
                ' wydzielenie tylko roku
                _Ret = X(intRokInSplit - 1).Substring(0, 4)
            Else
                For iC = 0 To X.GetUpperBound(0)
                    If X(iC).Contains("-") Then
                        X = Split(X(iC), "-")
                        If X.GetUpperBound(0) > 1 AndAlso blnFullDate Then
                            dtm = DateSerial(CInt(X(0)), CInt(X(1)), CInt(X(2).Substring(0, 2)))
                            _Ret = Format(dtm.Date, "yyyy-MM-dd")
                        Else
                            dtm = DateSerial(CInt(X(0)), CInt(X(1).Substring(0, 2)), 1)
                            _Ret = Format(dtm.Date, "yyyy-MM")
                        End If
                        Exit Try
                    End If
                Next iC
            End If
        Catch ex As Exception
        End Try
        Return _Ret
    End Function

    ' Określenie kwartału
    Function fKwartal(ByVal Miesiac As Integer) As Integer

        Return Int((Miesiac - 1) / 3) + 1
    End Function

    Function fKwartal(ByVal Dzien As Date) As Integer

        Return Int((Dzien.Month - 1) / 3) + 1
    End Function

    ' Ustawienie pozycji InputBox-a
    Function fPositionX() As Integer

        Try
            Dim FO As Form = Form.ActiveForm
            If FO Is Nothing Then FO = Application.OpenForms.Item(0)
            fPositionX = FO.Location.X + (FO.Size.Width - 450) \ 2
        Catch ex As Exception
            fPositionX = -1
        End Try
    End Function

    Function fPositionY() As Integer

        Try
            Dim FO As Form = Form.ActiveForm
            If FO Is Nothing Then FO = Application.OpenForms.Item(0)
            fPositionY = FO.Location.Y + (FO.Size.Height - 150) \ 2
        Catch ex As Exception
            fPositionY = -1
        End Try
    End Function

    ' Sprawdzenie układu kolumn arkusza danych w DataTable
    Function fSourceLayoutControl(ByVal dt As DataTable, ByVal Kontrolny As String, ByVal cns As OleDbConnection) As Boolean
        ' Source (nazwa pliku źródłowego),
        ' NrKol (kolumna pliku źródłowego)
        ' KolPodstawowa (bez spacji i #; nazwy używane przez program)
        ' Wariant1 - alternatywna, kontrolna nazwa
        ' Wariant2 - jw.
        ' Wariant3 - jw.
        Dim i, j, iC As Integer
        Dim dtT As New DataTable
        Dim dvT As New DataView
        Dim r As DataRow
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT * FROM tblSources WHERE Source = '" + Kontrolny + "' ORDER BY NrKol", cns)
        da.Fill(dtT)
        dvT = dtT.DefaultView
        Dim aCol(0 To dtT.Rows.Count - 1) As String
        Dim txt As String
        Try
            ' porównanie kolumn
            For iC = 0 To dtT.Rows.Count - 1
                r = dtT.Rows(iC)
                For i = 0 To dt.Columns.Count - 1
                    txt = dt.Columns(i).ColumnName.ToString.Replace("#", "").Replace(" ", "").ToUpper
                    If r("KolPodstawowa").ToString.ToUpper.Equals(txt) Then
                        If Not (CInt(r("NrKol").ToString) - 1).Equals(i) Then
                            Throw New ArgumentException("Zmieniony układ kolumn")
                        Else
                            dt.Columns(i).ColumnName = r("KolPodstawowa").ToString
                            aCol(iC) = txt
                            GoTo Dalej
                        End If
                    Else
                        ' przeszukaj pomocnicze kolumny
                        For j = 1 To 3
                            If r(j + 2).ToString.ToUpper.Equals(txt) Then
                                If Not (CInt(r("NrKol").ToString) - 1).Equals(i) Then
                                    Throw New ArgumentException("Zmieniony układ kolumn")
                                Else
                                    dt.Columns(i).ColumnName = r("KolPodstawowa").ToString
                                    aCol(iC) = txt
                                    GoTo Dalej
                                End If
                            End If
                        Next j
                    End If
                Next i
Dalej:
            Next iC
            For iC = 0 To aCol.GetUpperBound(0)
                If String.IsNullOrEmpty(aCol(iC)) Then
                    Throw New ArgumentException("Układ kolumn niezgodny z założeniami")
                End If
            Next iC
            Return True
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, Kontrolny, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Return False
        Finally
            dtT = Nothing
            dvT = Nothing
            da = Nothing
        End Try
    End Function

    ' Konwersja str - znak na końcu na double
    Function fStr2Dbl(ByVal strLiczba As String) As Double

        Try
            Dim strDbl As String = strLiczba.Replace(Chr(32), "").Replace(Chr(160), "")
            If strDbl.EndsWith("-") Then _
                strDbl = String.Concat("-", strDbl.Replace("-", ""))
            Return CDbl(strDbl)
        Catch ex As Exception
            Return 0
        End Try
    End Function

    ' Konwersja str - znak na końcu na integer
    Function fStr2Int(ByVal strLiczba As String) As Integer

        Try
            Dim strDbl As String = strLiczba.Replace(" ", "")
            If strDbl.EndsWith("-") Then _
                strDbl = String.Concat("-", strDbl.Replace("-", ""))
            Return CInt(strDbl)
        Catch ex As Exception
            Return 0
        End Try
    End Function

    ' Czy ten sam miesiąc po dodaniu dni
    Function fTenSamMiesiac(ByVal dmDate As DateTime, _
                            ByVal Dni As Integer, _
                            Optional ByRef Dzien As String = "") As Boolean

        Try
            Dim dm As Date = dmDate.AddDays(Dni)
            Dzien = Format(dm.Day, "dd")
            If dmDate.Year.Equals(dm.Year) Then Return dmDate.Month.Equals(dm.Month)
        Catch ex As Exception
        End Try
        Return False
    End Function

    ' Zastępuje polskie litery
    Function fZastapPolskie(ByVal Tekst As String) As String

        Dim iC, iB As Integer
        Dim newTekst As String = ""
        Const PODMIANA As String = "ACELNOSZZacelnoszz"
        Const ORYGINAL As String = "ĄĆĘŁŃÓŚŹŻąćęłńóśźż"
        Try
            For iC = 0 To Tekst.Length - 1
                iB = ORYGINAL.IndexOf(Tekst.Chars(iC))
                If iB.Equals(-1) Then
                    newTekst += Tekst.Chars(iC)
                Else
                    newTekst += PODMIANA.Chars(iB)
                End If
            Next iC
        Catch ex As Exception
        End Try
        Return newTekst
    End Function

End Module
