﻿
' Created   2013-08-21 by KBrzozowski
' Modified: 2015-03-10

Imports System.IO
Imports System.Data.OleDb
Imports XL = Microsoft.Office.Interop.Excel
Imports CM = ClHelpFul.CenterMsg
Imports CP = ClHelpFul.Pomocne

Public Class frmMain

#Region "Deklaracje"

    Public WybranaTabela As String
    Public cns As New OleDbConnection
    Public Okres As String = ""
    Private WithEvents Initial As TextBox
    Private Const Path2Xls As String = "W:\Costs\CHO_DZIENNY\"
    Private Const XlsName As String = "CHO_Monitor_Dzienny.xls"

#End Region ' Deklaracje

#Region "Obsługa frmMain"

    Private Sub Main_Load() Handles MyBase.Load

        Try
            pCheckVersion()
            With My.Settings
                If .Sciezka.LastIndexOf(Path.DirectorySeparatorChar).Equals(0) Then
                    .Sciezka += Path.DirectorySeparatorChar
                End If
                If Not My.Computer.FileSystem.FileExists(.Sciezka + .Baza) Then
                    Dim opf As New OpenFileDialog
                    With opf
                        .InitialDirectory = My.Settings.Sciezka
                        .Multiselect = False
                        .FileName = "dbCHOMonitor"
                        .Filter = "(*.mdb)|*.mdb"
                        If .ShowDialog().Equals(Windows.Forms.DialogResult.OK) Then
                            My.Settings.Baza = .SafeFileName
                            My.Settings.Sciezka = .FileName.Replace(My.Settings.Baza, "")
                        Else
                            KoniecProgramu()
                        End If
                    End With
                End If
                Me.Size = .scrMain
                Me.Location = .StartOfMain
                Me.dtpOkres.Value = CDate(.Okres)
                Me.dtpOkres.Size = .OkresSize
                Me.dtpOkres.Location = New Point(Me.Width - Me.dtpOkres.Width - 15, 0)
                Okres = .Okres
            End With
            OdswiezHelp()
            ' wybór połączenia
            SetPolaczenie()
            With Me.TimerOfDepData
                .Interval = 600000
                .Enabled = My.Settings.KontrolaKompletnosci
                If .Enabled Then .Start()
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            KoniecProgramu()
        End Try
    End Sub

    Private Sub MainFinish() Handles Me.FormClosing

        Koniec()
    End Sub

    Private Sub Koniec()

        Dim c As New ClADO.CompareMdb
        With My.Settings
            c.KopiaZapasowa(.Sciezka, .Baza, .Myk, "C:\CHO Monitoring\Archiwum\", , 7)
        End With
        With My.Settings
            .StartOfMain = Me.Location
            .scrMain = Me.Size
            .Save()
        End With
    End Sub

    Private Sub KoniecProgramu() Handles tsmKoniec.Click

        Koniec()
        Me.Dispose()
        GC.Collect()
        End
    End Sub

    Private Sub OdswiezHelp()

        'CP.CompareFileDate("W:\Controlling\VBA\CHO Monitoring\CHO_Monitor_Dzienny.chm", _
        '                   "C:\CHO Monitoring\Help\CHO_Monitor_Dzienny.chm")
    End Sub

    Private Sub SetPolaczenie()

        Dim m_Ado As New ClADO.ADOGet
        Try
            With m_Ado
                .Baza = My.Settings.Baza
                .Folder = My.Settings.Sciezka
                .Haslo = My.Settings.Myk
                If Not .OpenData(True) Then _
                    Throw New ArgumentException("Brak połączenia z plikiem bazy danych [" + My.Settings.Baza + "]")
                Me.cns.ConnectionString = .CN.ConnectionString
            End With
        Catch ex As Exception
            Throw New ArgumentException(ex.Message)
        Finally
            m_Ado = Nothing
        End Try
    End Sub

    ' Niezbędne do uruchomienia hasła
    Private Sub Initial_TextChanged() Handles Initial.TextChanged

        If Initial.Text.Equals("1") Then Return
        Initial.Text = "1"
    End Sub

    ' Pierwsze wyświetlenie ekranu głównego
    Private Sub frmMain_Shown() Handles MyBase.Shown

        If Initial Is Nothing Then
            Initial = New TextBox
            Initial.Text = "0"
        End If
    End Sub

    Private Sub MainMenu_Click() _
                                Handles tsmDane.Click, _
                                        tsmPomocne.Click
        With Me
            With .txtKomunikat
                .Clear()
                .Visible = False
            End With
        End With
    End Sub

    Private Sub Okres_ValueChanged() Handles dtpOkres.ValueChanged

        With My.Settings
            Try
                If Me.dtpOkres.Value > Now Then Throw New ArgumentException
                .Okres = Me.dtpOkres.Value.ToShortDateString
            Catch ex As Exception
                Me.dtpOkres.Value = Now
                .Okres = Now.ToShortDateString
            End Try
            Okres = .Okres
            If .KontrolaKompletnosci Then Me.TimerOfDepData.Start()
        End With
    End Sub

    Private Sub frmMain_ResizeEnd() Handles Me.ResizeEnd

        Me.dtpOkres.Location = New Point(Me.Width - Me.dtpOkres.Width - 15, 0)
    End Sub

#End Region ' Obsługa frmMain

#Region "Menu dzienne"

    Private Sub Pobierz() Handles tsmPobierz.Click

        Dim m_Path As String = My.Settings.Sciezka.Replace("DataBase", "Dane")
        Dim m_PathPl As String = "W:\Costs\CELSA\Wykon" + Me.dtpOkres.Value.Year.ToString _
                            + "\Plan_" + Me.dtpOkres.Value.Year.ToString + "\"
        Dim m_File() As String
        Dim m_Ext As String = ".xl*"
        Dim da As OleDbDataAdapter
        Dim dt As New DataTable, dtC As New DataTable, dtE As New DataTable, dtR As New DataTable
        Dim dtP As New DataTable, dtT As New DataTable
        Dim dvE, dvP As New DataView
        Dim r As DataRow = Nothing
        Dim oDane As Object
        Dim i, j, l, m, s, m_Poz As Integer
        Dim dtm1 As Date
        Dim m_Skl As String = ""
        Dim m_ID As String = ""
        Dim m_ID2, _C As String
        Dim m_KKL As String
        Dim m_Rabat As Double
        Dim aCtrl, aCtrl2, aCtrl3 As Array
        Dim vPoz As Array
        Dim m_Dbl(6) As Double
        Dim tr As OleDbTransaction = Nothing
        Dim cmd As New OleDbCommand
        Const CO_ACTION As String = "Pobieranie danych"
        Try
            Using cn As New OleDbConnection(Me.cns.ConnectionString)
                Try
                    cn.Open()
                    tr = cn.BeginTransaction
                    With cmd
                        .Connection = cn
                        .Transaction = tr
                        .CommandText = "SELECT * FROM tblExpedition WHERE DataWejscia = #" + Me.dtpOkres.Value.ToShortDateString + "#"
                        dtE.Load(.ExecuteReader)
                        .CommandText = "SELECT * FROM tblProduction WHERE DataWejscia = #" + Me.dtpOkres.Value.ToShortDateString + "#"
                        dtP.Load(.ExecuteReader)
                        .CommandText = "SELECT * FROM tblRabaty"
                        dtR.Load(.ExecuteReader)
                        If Not dtE.Rows.Count.Equals(0) OrElse Not dtP.Rows.Count.Equals(0) Then
                            Using New CM
                                If MessageBox.Show("Istniejące dane za " + Okres + " zostaną nadpisane." + vbCrLf _
                                                   + "Kontynuujesz?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(vbNo) Then Return
                            End Using
                        End If
                        dtE = New DataTable
                        dtP = New DataTable
                        ' usuwanie poprzednich zapisów dziennych
                        .CommandText = "DELETE FROM tblExpedition WHERE DataWejscia = #" + Me.dtpOkres.Value.ToShortDateString + "#"
                        .ExecuteNonQuery()
                        .CommandText = "DELETE FROM tblProduction WHERE DataWejscia = #" + Me.dtpOkres.Value.ToShortDateString + "#"
                        .ExecuteNonQuery()
                        .CommandText = "SELECT TOP 1 * FROM tblExpedition"
                        dt.Load(.ExecuteReader)
                        dtE = dt.Clone
                        dvE = dtE.DefaultView
                        dvE.Sort = "KolRaportu"
                        dt = New DataTable
                        .CommandText = "SELECT TOP 1 * FROM tblProduction"
                        dt.Load(.ExecuteReader)
                        dtP = dt.Clone
                        dvP = dtP.DefaultView
                        dvP.Sort = "KolRaportu"
                        dt = New DataTable
                    End With
                    FillStatusBar(, CO_ACTION)
                    m = Me.dtpOkres.Value.Month + 3
                    _C = CP.GetColumnLetter(m)
                    ' Otworzenie pliku programowego
                    Dim XG As New Cl4Excel.GetFromXls(Path2Xls + XlsName, "Expedition", , "ExpPlanOp")
                    With XG
                        .Data2Array()
                        aCtrl = .Dane
                    End With
                    ' Pobieranie planu operacyjnego
                    With New Cl4Excel.GetFromXls(m_PathPl + "War_" + Me.dtpOkres.Value.Year.ToString + "op.xls", "Spread", "D580:O582")
                        .Data2Array()
                        vPoz = .Dane
                        For i = aCtrl.GetLowerBound(1) To aCtrl.GetUpperBound(1)
                            If aCtrl(1, i) Is Nothing Then GoTo NextArk
                            m_File = Split(aCtrl(1, i), "#")
                            For j = m_File.GetLowerBound(0) To m_File.GetUpperBound(0)
                                .Arkusz = m_File(j)
                                .Zakres = _C + "3:" + _C + "80"
                                .Data2Array()
                                oDane = .Dane
                                Dodaj2Tbl(dvE, i + 3, m_Poz)
                                Try
                                    dvE(m_Poz)("PlanDomesticMg") = CP.Str2Dbl(dvE(m_Poz)("PlanDomesticMg").ToString) + oDane(2, 1)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("PlanExportMg") = CP.Str2Dbl(dvE(m_Poz)("PlanExportMg").ToString) + oDane(6, 1)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("PlanDomesticPLN") = CP.Str2Dbl(dvE(m_Poz)("PlanDomesticPLN").ToString) + oDane(26, 1)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("PlanExportPLN") = CP.Str2Dbl(dvE(m_Poz)("PlanExportPLN").ToString) + oDane(30, 1)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("PlanTranspDomesticPLN") = CP.Str2Dbl(dvE(m_Poz)("PlanTranspDomesticPLN").ToString) + oDane(62, 1)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("PlanTranspExportPLN") = CP.Str2Dbl(dvE(m_Poz)("PlanTranspExportPLN").ToString) + oDane(66, 1)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("PlanOtherDomesticPLN") = oDane(74, 1)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("PlanOtherExportPLN") = oDane(78, 1)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("PlanCenaPLN1") = vPoz(1, m - 3)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("PlanCenaPLN2") = vPoz(2, m - 3)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("PlanCenaPLN3") = vPoz(3, m - 3)
                                Catch ex As Exception
                                End Try
                            Next j
NextArk:
                        Next i
                        .Zwolnij()
                    End With
                    aCtrl = Nothing
                    With XG
                        .NazwaObszaru = "ExpGrMater"
                        .Data2Array()
                        aCtrl = .Dane
                        .NazwaObszaru = "ExpMagazyny"
                        .Data2Array()
                        aCtrl2 = .Dane
                        .NazwaObszaru = "ExpGrKalk"
                        .Data2Array()
                        aCtrl3 = .Dane
                        .Obszar = Nothing
                        .NazwaObszaru = Nothing
                    End With
                    With New Cl4Excel.GetFromXls(m_Path + "BWD_Transport.xls", "BWD", "F15")
                        .Data2Array()
                        vPoz = .Dane
                        .Zwolnij()
                    End With
                    If vPoz Is Nothing OrElse vPoz(1, 2) Is Nothing Then GoTo Wysylka
                    FillStatusBar(vPoz.GetUpperBound(0), "Dane o transporcie...")
                    i = 2
                    Do Until i > vPoz.GetUpperBound(0)
                        m_ID = vPoz(i, 4)
                        If String.IsNullOrEmpty(m_ID) Then Exit Do
                        Do While i <= vPoz.GetUpperBound(0) AndAlso vPoz(i, 4).Equals(m_ID)
                            m_ID2 = vPoz(i, 6)
                            Do While i <= vPoz.GetUpperBound(0) _
                                    AndAlso vPoz(i, 4).Equals(m_ID) _
                                    AndAlso vPoz(i, 6).Equals(m_ID2)
                                m_Skl = vPoz(i, 14)
                                m_Dbl(0) = 0
                                m_Dbl(1) = 0
                                Do While i <= vPoz.GetUpperBound(0) _
                                        AndAlso vPoz(i, 4).Equals(m_ID) _
                                        AndAlso vPoz(i, 6).Equals(m_ID2) _
                                        AndAlso vPoz(i, 14).Equals(m_Skl)
                                    Try
                                        m = CInt(vPoz(i, 2))
                                        If Not vPoz(i, 19) Is Nothing Then
                                            If m.Equals(1) Then
                                                m_Dbl(0) += vPoz(i, 19)
                                            Else
                                                m_Dbl(1) += vPoz(i, 19)
                                            End If
                                        End If
                                    Catch ex As Exception
                                    End Try
                                    If (i Mod 10).Equals(0) Then
                                        Me.tspbMain.Value = i
                                        Application.DoEvents()
                                    End If
                                    i += 1
                                Loop
                                If m_ID Is Nothing AndAlso m_ID2 Is Nothing Then _
                                    GoTo Pusty1
                                m_Skl = m_Skl.Replace("C35/", "")
                                For j = 1 To aCtrl.GetUpperBound(1)
                                    If Not aCtrl(1, j) Is Nothing Then
                                        If aCtrl(1, j).ToString.IndexOf(m_ID.ToUpper + "_") > -1 Then
                                            If m_Skl.Equals("JSWH") Then
                                                If Not String.IsNullOrEmpty(aCtrl2(1, j)) AndAlso aCtrl2(1, j).ToString.IndexOf(m_Skl) > -1 Then
                                                    s = j + 3
                                                    GoTo PoSzukaniu1
                                                End If
                                            Else
                                                If String.IsNullOrEmpty(aCtrl2(1, j)) Then
                                                    s = j + 3
                                                    GoTo PoSzukaniu1
                                                End If
                                            End If
                                        End If
                                    End If
                                Next j
                                For j = 1 To aCtrl3.GetUpperBound(1)
                                    If Not String.IsNullOrEmpty(aCtrl3(1, j)) AndAlso m_ID2.Length > 2 Then
                                        If aCtrl3(1, j).ToString.IndexOf(m_ID2.Substring(0, 3)) > -1 Then
                                            s = j + 3
                                            GoTo PoSzukaniu1
                                        End If
                                    End If
                                Next j
                                For j = 1 To aCtrl3.GetUpperBound(1)
                                    If Not String.IsNullOrEmpty(aCtrl3(1, j)) AndAlso m_ID2.Length > 1 Then
                                        If aCtrl3(1, j).ToString.IndexOf(m_ID2.Substring(0, 2)) > -1 Then
                                            s = j + 3
                                            GoTo PoSzukaniu1
                                        End If
                                    End If
                                Next j
                                s = 18
PoSzukaniu1:
                                Dodaj2Tbl(dvE, s, m_Poz)
                                Try
                                    dvE(m_Poz)("TranspDomesticPLN") = CP.Str2Dbl(dvE(m_Poz)("TranspDomesticPLN").ToString) + m_Dbl(0)
                                Catch ex As Exception
                                End Try
                                Try
                                    dvE(m_Poz)("TranspExportPLN") = CP.Str2Dbl(dvE(m_Poz)("TranspExportPLN").ToString) + m_Dbl(1)
                                Catch ex As Exception
                                End Try
                            Loop
                        Loop
Pusty1:
                    Loop
Wysylka:
                    With New Cl4Excel.GetFromXls(m_Path + "BWD_Wysyłka.xls", "BWD", "F15")
                        .Data2Array()
                        vPoz = .Dane
                        .Zwolnij()
                    End With
                    If vPoz Is Nothing OrElse vPoz(1, 2) Is Nothing Then GoTo ZWMM075
                    FillStatusBar(vPoz.GetUpperBound(0), "Dane o wysyłce...")
                    i = 2
                    Try
                        Do Until i > vPoz.GetUpperBound(0)
                            m_Rabat = 0
                            m_ID = vPoz(i, 8) 'grupa materiałowa
                            'm_KKL = vPoz(i, 19) 'KKL dostawcy
                            'If Not (dtR.Select("GrupaMat = '" + m_ID + "' and Klient='" + m_KKL + "'").Length = 0) Then
                            '    m_Rabat = dtR.Select("GrupaMat = '" + m_ID + "' and Klient='" + m_KKL + "'")(0)(6)
                            '    If dtR.Select("GrupaMat = '" + m_ID + "' and Klient='" + m_KKL + "'")(0)(7) = "EUR" Then
                            '        m_Rabat = m_Rabat * My.Settings.KursEURO
                            '    Else
                            '        m_Rabat = m_Rabat
                            '    End If
                            'Else
                            '    m_Rabat = 0
                            'End If
                            If String.IsNullOrEmpty(m_ID) Then Exit Do
                            Do While i <= vPoz.GetUpperBound(0) AndAlso vPoz(i, 8).Equals(m_ID)
                                m_ID2 = vPoz(i, 10) ' grupa kalkulacyjna 
                                Do While i <= vPoz.GetUpperBound(0) _
                                        AndAlso vPoz(i, 8).Equals(m_ID) _
                                        AndAlso vPoz(i, 10).Equals(m_ID2)
                                    m_Skl = vPoz(i, 12)
                                    For j = 0 To 3
                                        m_Dbl(j) = 0
                                    Next j
                                    Do While i <= vPoz.GetUpperBound(0) _
                                            AndAlso Not vPoz(i, 8) Is Nothing _
                                            AndAlso vPoz(i, 8).Equals(m_ID) _
                                            AndAlso vPoz(i, 10).Equals(m_ID2) _
                                            AndAlso vPoz(i, 12).Equals(m_Skl)
                                        Try
                                            m = CInt(vPoz(i, 6))
                                            If Not vPoz(i, 25) Is Nothing Then
                                                If m.Equals(1) Then
                                                    m_Dbl(0) += vPoz(i, 25)
                                                Else
                                                    m_Dbl(2) += vPoz(i, 25)
                                                End If
                                            End If
                                            If Not vPoz(i, 26) Is Nothing Then
                                                'obliczanie rabatu
                                                m_Rabat = 0
                                                m_KKL = vPoz(i, 19) 'KKL dostawcy
                                                If Not (dtR.Select("GrupaMat = '" + m_ID + "' and Klient='" + m_KKL + "'").Length = 0) Then
                                                    m_Rabat = dtR.Select("GrupaMat = '" + m_ID + "' and Klient='" + m_KKL + "'")(0)(5)
                                                    If dtR.Select("GrupaMat = '" + m_ID + "' and Klient='" + m_KKL + "'")(0)(6) = "EUR" Then
                                                        m_Rabat = m_Rabat * My.Settings.KursEURO
                                                    Else
                                                        m_Rabat = m_Rabat
                                                    End If
                                                Else
                                                    m_Rabat = 0
                                                End If
                                                If m.Equals(1) Then
                                                    m_Dbl(1) += (vPoz(i, 26) - (m_Rabat * vPoz(i, 25) / 1000))
                                                Else
                                                    m_Dbl(3) += (vPoz(i, 26) - (m_Rabat * vPoz(i, 25) / 1000))
                                                End If
                                            End If
                                        Catch ex As Exception
                                        End Try
                                        If (i Mod 10).Equals(0) Then
                                            Me.tspbMain.Value = i
                                            Application.DoEvents()
                                        End If
                                        i += 1
                                    Loop
                                    If String.IsNullOrEmpty(m_ID) _
                                        AndAlso String.IsNullOrEmpty(m_ID2) Then _
                                        GoTo Pusty2
                                    If m_ID2.Trim.Equals("#") Then
                                        s = 18
                                        GoTo PoSzukaniu2
                                    End If
                                    For j = 1 To aCtrl.GetUpperBound(1)
                                        If Not aCtrl(1, j) Is Nothing Then
                                            If aCtrl(1, j).ToString.Contains(m_ID.ToUpper + "_") Then
                                                If m_Skl.Equals("JSWH") Then
                                                    If Not aCtrl2(1, j) Is Nothing AndAlso aCtrl2(1, j).ToString.Contains(m_Skl) Then
                                                        s = j + 3
                                                        GoTo PoSzukaniu2
                                                    End If
                                                Else
                                                    If String.IsNullOrEmpty(aCtrl2(1, j)) Then
                                                        s = j + 3
                                                        GoTo PoSzukaniu2
                                                    End If
                                                End If
                                            End If
                                        End If
                                    Next j
                                    For j = 1 To aCtrl3.GetUpperBound(1)
                                        If Not aCtrl3(1, j) Is Nothing AndAlso m_ID2.Length > 2 Then
                                            If aCtrl3(1, j).ToString.Contains(m_ID2.Substring(0, 3)) Then
                                                s = j + 3
                                                GoTo PoSzukaniu2
                                            End If
                                        End If
                                    Next j
                                    For j = 1 To aCtrl3.GetUpperBound(1)
                                        If Not aCtrl3(1, j) Is Nothing AndAlso m_ID2.Length > 1 Then
                                            If aCtrl3(1, j).ToString.Contains(m_ID2.Substring(0, 2)) Then
                                                s = j + 3
                                                GoTo PoSzukaniu2
                                            End If
                                        End If
                                    Next j
                                    s = 18
PoSzukaniu2:
                                    Dodaj2Tbl(dvE, s, m_Poz)
                                    If Not s.Equals(18) Then
                                        Try
                                            dvE(m_Poz)("DomesticMg") = CP.Str2Dbl(dvE(m_Poz)("DomesticMg").ToString) + m_Dbl(0) / 1000
                                        Catch ex As Exception
                                        End Try
                                        Try
                                            dvE(m_Poz)("ExportMg") = CP.Str2Dbl(dvE(m_Poz)("ExportMg").ToString) + m_Dbl(2) / 1000
                                        Catch ex As Exception
                                        End Try
                                    End If
                                    Try
                                        dvE(m_Poz)("DomesticPLN") = CP.Str2Dbl(dvE(m_Poz)("DomesticPLN").ToString) + m_Dbl(1)
                                    Catch ex As Exception
                                    End Try
                                    Try
                                        dvE(m_Poz)("ExportPLN") = CP.Str2Dbl(dvE(m_Poz)("ExportPLN").ToString) + m_Dbl(3)
                                    Catch ex As Exception
                                    End Try
                                Loop
                            Loop
Pusty2:
                        Loop
                    Catch ex As Exception
                    End Try
ZWMM075:
                    Dim m_FI() As FileInfo
                    m_FI = New DirectoryInfo(m_Path).GetFiles("ZWMM075*.html")
                    With XG
                        .Arkusz = "Production"
                        .NazwaObszaru = "ProdMagazyny"
                        .Data2Array()
                        aCtrl = .Dane
                        .Zwolnij()
                    End With
                    For i = 0 To m_FI.GetUpperBound(0)
                        oDane = Nothing
                        With New Cl4Excel.GetFromXls(m_FI(i).FullName, , , , , True)
                            .HTMLToObjectViaExcel()
                            oDane = .Dane
                        End With
                        Dim K() As Integer = Nothing
                        If oDane Is Nothing Then GoTo Next075
                        If Not CP.SourceLayoutControl(oDane, "ZWMM075", Me.cns, K) Then GoTo Next075
                        dtm1 = CP.Conv2Date(oDane(2, K(3)))
                        If Not IsDate(dtm1) Then
                            Using New CM
                                MessageBox.Show(m_FI(i).Name + ": Nierozpoznawalny format daty końcowej", "ZWMM075", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End Using
                            GoTo Next075
                        End If
                        If Not dtm1.Equals(Me.dtpOkres.Value) Then
                            Using New CM
                                MessageBox.Show(m_FI(i).Name + ": Nieodpowiedni zakres danych (data końcowa różna od daty przetwarzania)", "ZWMM075", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End Using
                            GoTo Next075
                        End If
                        FillStatusBar(oDane.GetUpperBound(0), m_FI(i).Name)
                        j = 2
                        Do
                            For l = m_Dbl.GetLowerBound(0) To m_Dbl.GetUpperBound(0)
                                m_Dbl(l) = 0
                            Next l
                            m_Skl = oDane(j, K(0)).ToString.Trim
                            ' Ustal kolumnę
                            s = 0
                            For m = 1 To aCtrl.GetUpperBound(1)
                                If aCtrl(1, m).ToString.Contains(m_Skl) Then
                                    s = 3 + m
                                    Exit For
                                End If
                            Next m
                            Select Case m_Skl
                                Case "*", "", "ZZZZ"
                                Case oDane(j, K(0)).ToString.Trim
                                Case Else
                                    Using New CM
                                        If Not My.Settings.ExcludedStock.Contains(m_Skl) Then _
                                          MessageBox.Show("ZWMM075: Niezidentyfikowany magazyn [" + m_Skl + "]", _
                                                            m_FI(i).Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    End Using
                            End Select
                            Do While oDane(j, K(0)).ToString.Trim.Equals(m_Skl)
                                If s.Equals(0) Then GoTo PoUstaleniu
                                If oDane(j, K(1)).ToString.Trim.Equals("CHAT") Then
                                    m_Dbl(4) += CP.Str2Dbl(oDane(j, K(8)))
                                    m_Dbl(5) += CP.Str2Dbl(oDane(j, K(4)))
                                    m_Dbl(6) += CP.Str2Dbl(oDane(j, K(5)))
                                    GoTo PoUstaleniu
                                End If
                                ' tylko dla WOM
                                If s.Equals(9) Then m_Dbl(0) += CP.Str2Dbl(oDane(j, K(6)))
                                ' zuzycie Prasownia
                                If s.Equals(8) Then m_Dbl(0) += CP.Str2Dbl(oDane(j, K(7)))
                                ' zapas wsadu i prod gotowej
                                If oDane(j, K(1)).ToString.Trim.Equals("PAL") Then _
                                    m_Dbl(1) += CP.Str2Dbl(oDane(j, K(8))) : GoTo PoUstaleniu
                                If oDane(j, K(2)).ToString.StartsWith("JFI") Then _
                                    m_Dbl(1) += CP.Str2Dbl(oDane(j, K(8))) : GoTo PoUstaleniu
                                If s.Equals(7) Then
                                    If oDane(j, K(2)).ToString.StartsWith("JFL") Then
                                        m_Dbl(2) += CP.Str2Dbl(oDane(j, K(8)))
                                    Else
                                        m_Dbl(3) += CP.Str2Dbl(oDane(j, K(8)))
                                    End If
                                    GoTo PoUstaleniu
                                End If
                                If oDane(j, K(2)).ToString.StartsWith("JFR") Then _
                                    m_Dbl(3) += CP.Str2Dbl(oDane(j, K(8))) : GoTo PoUstaleniu
                                m_Dbl(2) += CP.Str2Dbl(oDane(j, K(8)))
PoUstaleniu:
                                j += 1
                                If j > oDane.GetUpperBound(0) Then Exit Do
                                If (j Mod 10).Equals(0) Then
                                    Me.tspbMain.Value = j
                                    Application.DoEvents()
                                End If
                            Loop
                            If s.Equals(0) Then GoTo PoZapisie
                            If Not m_Dbl(0).Equals(0) Then
                                Dodaj2Tbl(dvP, s, m_Poz)
                                If m_Skl.Equals("JKMF") Then
                                    dvP(m_Poz)("Prod4SaleMG") = CP.Str2Dbl(dvP(m_Poz)("Prod4SaleMG").ToString) + m_Dbl(0) / 1000
                                ElseIf s.Equals(8) Then
                                    dvP(m_Poz)("ChargeConMg") = CP.Str2Dbl(dvP(m_Poz)("ChargeConMg").ToString) + m_Dbl(0) / 1000
                                Else
                                    dvP(m_Poz)("SemiProdMG") = CP.Str2Dbl(dvP(m_Poz)("SemiProdMG").ToString) + m_Dbl(0) / 1000
                                End If
                            End If
                            If Not m_Dbl(1).Equals(0) Then
                                If s > 6 Then
                                    Dodaj2Tbl(dvP, 8, m_Poz)
                                Else
                                    Dodaj2Tbl(dvP, s, m_Poz)
                                End If
                                dvP(m_Poz)("ChargeStockMg") = CP.Str2Dbl(dvP(m_Poz)("ChargeStockMg").ToString) + m_Dbl(1) / 1000
                            End If
                            Dodaj2Tbl(dvP, s, m_Poz)
                            If Not m_Dbl(2).Equals(0) Then _
                                    dvP(m_Poz)("ProdStockMg") = CP.Str2Dbl(dvP(m_Poz)("ProdStockMg").ToString) + m_Dbl(2) / 1000
                            If Not m_Dbl(3).Equals(0) Then _
                                dvP(m_Poz)("RejectStockMg") = CP.Str2Dbl(dvP(m_Poz)("RejectStockMg").ToString) + m_Dbl(3) / 1000
                            If Not m_Dbl(4).Equals(0) Then _
                                dvP(m_Poz)("ScrapSAPMG") = CP.Str2Dbl(dvP(m_Poz)("ScrapSAPMG").ToString) + m_Dbl(4) / 1000
                            If Not m_Dbl(5).Equals(0) Then
                                dvP(m_Poz)("ScrapSAPPurMG") = CP.Str2Dbl(dvP(m_Poz)("ScrapSAPPurMG").ToString) + m_Dbl(5) / 1000
                                dvP(m_Poz)("ScrapSAPPurPLN") = CP.Str2Dbl(dvP(m_Poz)("ScrapSAPPurPLN").ToString) + m_Dbl(6)
                            End If
PoZapisie:
                        Loop Until j > oDane.GetUpperBound(0)
Next075:
                    Next i
                    ' Pobieranie danych wydzialowych
                    Dim aKolumny As Array = Nothing
                    With cmd
                        .CommandText = "SELECT * FROM tblKontrola ORDER BY ID"
                        dtC.Load(.ExecuteReader)
                    End With
WOM:
                    m = Me.dtpOkres.Value.Day
                    FillStatusBar(0, "Dane wydziałowe WOM...")
                    m_Path = "W:\CHO Dzienny\Dane_" + Me.dtpOkres.Value.Year.ToString + "\"
                    With New Cl4Excel.GetFromXls(m_Path + "WOM_" + Me.dtpOkres.Value.Year.ToString + ".xls", "Mc" + Okres.Substring(5, 2), "A7")
                        .Data2Array()
                        vPoz = .Dane
                        .Zwolnij()
                    End With
                    If vPoz Is Nothing Then
                        Using New CM
                            MessageBox.Show("Błąd pobrania danych WOMtsmPobierz", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        GoTo Prasownia
                    End If
                    For l = m_Dbl.GetLowerBound(0) To m_Dbl.GetUpperBound(0)
                        m_Dbl(l) = 0
                    Next l
                    SprawdzKontrolne(dtC.Rows(8), aKolumny)
                    If BrakDanychDziennych(vPoz, aKolumny, "WOM", ) Then GoTo Prasownia
                    Dodaj2Tbl(dvP, 9, m_Poz)
                    Try
                        dvP(m_Poz)("Utylizacja") = vPoz(m, 6)
                    Catch ex As Exception
                        dvP(m_Poz)("Utylizacja") = 0
                    End Try
                    Try
                        dvP(m_Poz)("UtylizacjaNarastaj") = vPoz(m, 7)
                    Catch ex As Exception
                        dvP(m_Poz)("UtylizacjaNarastaj") = 0
                    End Try
Prasownia:
                    FillStatusBar(0, "Dane wydziałowe Prasowni...")
                    With New Cl4Excel.GetFromXls(m_Path + "Prasownia_" + Me.dtpOkres.Value.Year.ToString + ".xls", "Mc" + Okres.Substring(5, 2), "A7")
                        .Data2Array()
                        vPoz = .Dane
                        .Zwolnij()
                    End With
                    If vPoz Is Nothing Then
                        Using New CM
                            MessageBox.Show("Błąd pobrania danych Prasowni", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        GoTo StalowniaZWK
                    End If
                    For l = m_Dbl.GetLowerBound(0) To m_Dbl.GetUpperBound(0)
                        m_Dbl(l) = 0
                    Next l
                    SprawdzKontrolne(dtC.Rows(1), aKolumny)
                    If BrakDanychDziennych(vPoz, aKolumny, "Prasownia") Then GoTo StalowniaZWK
                    i = 1
                    Do Until CInt(vPoz(i, 1)) > m
                        m_Dbl(0) += vPoz(i, 10)
                        m_Dbl(1) += vPoz(i, 11)
                        m_Dbl(2) += vPoz(i, 7)
                        i += 1
                        If i.Equals(32) Then Exit Do
                    Loop
                    Dodaj2Tbl(dvP, 8, m_Poz)
                    dvP(m_Poz)("Prod4SaleMG") = m_Dbl(0)
                    dvP(m_Poz)("SemiProdMG") = m_Dbl(1)
                    dvP(m_Poz)("Consum2KWh") = m_Dbl(2)
                    Try
                        dvP(m_Poz)("Utylizacja") = vPoz(m, 16)
                    Catch ex As Exception
                    End Try
                    Try
                        dvP(m_Poz)("UtylizacjaNarastaj") = vPoz(m, 17)
                    Catch ex As Exception
                    End Try
StalowniaZWK:
                    FillStatusBar(0, "Dane wydziałowe Stalowni ZWK...")
                    With New Cl4Excel.GetFromXls(m_Path + "StalowniaZWK_" + Me.dtpOkres.Value.Year.ToString + ".xls", "Mc" + Okres.Substring(5, 2), "A7")
                        .Data2Array()
                        vPoz = .Dane
                        .Zwolnij()
                    End With
                    If vPoz Is Nothing Then
                        Using New CM
                            MessageBox.Show("Błąd pobrania danych Stalowni ZWK", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        GoTo StalowniaZWW
                    End If
                    For l = m_Dbl.GetLowerBound(0) To m_Dbl.GetUpperBound(0)
                        m_Dbl(l) = 0
                    Next l
                    SprawdzKontrolne(dtC.Rows(2), aKolumny)
                    If BrakDanychDziennych(vPoz, aKolumny, "StalowniaZWK") Then GoTo StalowniaZWW
                    i = 1
                    Do Until CInt(vPoz(i, 1)) > m
                        m_Dbl(0) += vPoz(i, 2)
                        m_Dbl(1) += vPoz(i, 3) + vPoz(i, 5)
                        m_Dbl(2) += vPoz(i, 4)
                        m_Dbl(3) += vPoz(i, 7)
                        m_Dbl(4) += vPoz(i, 5)
                        i += 1
                        If i.Equals(32) Then Exit Do
                    Loop
                    Dodaj2Tbl(dvP, 7, m_Poz)
                    dvP(m_Poz)("ZuzycieZlomuMG") = m_Dbl(0)
                    dvP(m_Poz)("SemiProdMG") = m_Dbl(1)
                    dvP(m_Poz)("Prod4SaleMG") = m_Dbl(2)
                    dvP(m_Poz)("Consum1KWh") = m_Dbl(3)
                    dvP(m_Poz)("StalPlynnaMG") = m_Dbl(4)
                    Try
                        dvP(m_Poz)("Utylizacja") = vPoz(m, 13)
                    Catch ex As Exception
                    End Try
                    Try
                        dvP(m_Poz)("UtylizacjaNarastaj") = vPoz(m, 14)
                    Catch ex As Exception
                    End Try
StalowniaZWW:
                    FillStatusBar(0, "Dane wydziałowe Stalowni ZWW...")
                    With New Cl4Excel.GetFromXls(m_Path + "StalowniaZWW_" + Me.dtpOkres.Value.Year.ToString + ".xls", "Mc" + Okres.Substring(5, 2), "A7")
                        .Data2Array()
                        vPoz = .Dane
                        .Zwolnij()
                    End With
                    If vPoz Is Nothing Then
                        Using New CM
                            MessageBox.Show("Błąd pobrania danych Stalowni ZWW", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        GoTo Walcownia1
                    End If
                    For l = m_Dbl.GetLowerBound(0) To m_Dbl.GetUpperBound(0)
                        m_Dbl(l) = 0
                    Next l
                    SprawdzKontrolne(dtC.Rows(3), aKolumny)
                    If BrakDanychDziennych(vPoz, aKolumny, "StalowniaZWW") Then GoTo Walcownia1
                    i = 1
                    Do Until CInt(vPoz(i, 1)) > m
                        m_Dbl(0) += vPoz(i, 2)
                        m_Dbl(1) += vPoz(i, 6)
                        m_Dbl(2) += vPoz(i, 7)
                        m_Dbl(3) += vPoz(i, 5)
                        m_Dbl(4) += vPoz(i, 16)
                        m_Dbl(5) += vPoz(i, 3)
                        i += 1
                        If i.Equals(32) Then Exit Do
                    Loop
                    Dodaj2Tbl(dvP, 4, m_Poz)
                    dvP(m_Poz)("ZuzycieZlomuMG") = m_Dbl(0)
                    dvP(m_Poz)("Consum3MG") = m_Dbl(1)
                    dvP(m_Poz)("Consum1KWh") = m_Dbl(2)
                    dvP(m_Poz)("SemiProdMG") = m_Dbl(3)
                    dvP(m_Poz)("Godziny") = m_Dbl(4)
                    dvP(m_Poz)("StalPlynnaMG") = m_Dbl(5)
                    Try
                        dvP(m_Poz)("Utylizacja") = vPoz(m, 20)
                    Catch ex As Exception
                    End Try
                    Try
                        dvP(m_Poz)("UtylizacjaNarastaj") = vPoz(m, 21)
                    Catch ex As Exception
                    End Try
Walcownia1:
                    For s = 1 To 2
                        FillStatusBar(0, "Dane wydziałowe Walcowni " + s.ToString + "...")
                        With New Cl4Excel.GetFromXls(m_Path + "Walcownia" + s.ToString + "_" + Me.dtpOkres.Value.Year.ToString + ".xls", "Mc" + Okres.Substring(5, 2), "A7")
                            .Data2Array()
                            vPoz = .Dane
                            .Zwolnij()
                        End With
                        If vPoz Is Nothing Then
                            Using New CM
                                MessageBox.Show("Błąd pobrania danych Walcowni " + s.ToString, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End Using
                            Exit For
                        End If
                        For l = m_Dbl.GetLowerBound(0) To m_Dbl.GetUpperBound(0)
                            m_Dbl(l) = 0
                        Next l
                        SprawdzKontrolne(dtC.Rows(3 + s), aKolumny)
                        If BrakDanychDziennych(vPoz, aKolumny, "Walcownia " + s.ToString) Then Exit For
                        i = 1
                        Do Until CInt(vPoz(i, 1)) > m
                            Try
                                m_Dbl(0) += vPoz(i, 2)
                            Catch ex As Exception
                            End Try
                            Try
                                m_Dbl(1) += vPoz(i, 3)
                            Catch ex As Exception
                            End Try
                            Try
                                m_Dbl(2) += vPoz(i, 4)
                            Catch ex As Exception
                            End Try
                            Try
                                m_Dbl(3) += vPoz(i, 6)
                            Catch ex As Exception
                            End Try
                            i += 1
                            If i.Equals(32) Then Exit Do
                        Loop
                        Dodaj2Tbl(dvP, 4 + s, m_Poz)
                        dvP(m_Poz)("ChargeConMg") = m_Dbl(0)
                        dvP(m_Poz)("Prod4SaleMG") = m_Dbl(1)
                        dvP(m_Poz)("Consum1KWh") = m_Dbl(2)
                        dvP(m_Poz)("Consum2KWh") = m_Dbl(3)
                        Try
                            dvP(m_Poz)("Utylizacja") = vPoz(m, 10)
                        Catch ex As Exception
                        End Try
                        Try
                            dvP(m_Poz)("UtylizacjaNarastaj") = vPoz(m, 11)
                        Catch ex As Exception
                        End Try
                    Next s
                    ' ZlomZWK:
                    FillStatusBar(0, "Dane o złomie ZWK...")
                    With New Cl4Excel.GetFromXls(m_Path + "ZłomZWK_" + Me.dtpOkres.Value.Year.ToString + ".xls", "Mc" + Okres.Substring(5, 2), "A7")
                        .Data2Array()
                        vPoz = .Dane
                        .Zwolnij()
                    End With
                    If vPoz Is Nothing Then
                        Using New CM
                            MessageBox.Show("Błąd pobrania danych o złomie ZWK", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        GoTo ZlomZWW
                    End If
                    For l = m_Dbl.GetLowerBound(0) To m_Dbl.GetUpperBound(0)
                        m_Dbl(l) = 0
                    Next l
                    SprawdzKontrolne(dtC.Rows(6), aKolumny)
                    If BrakDanychDziennych(vPoz, aKolumny, "ZłomZWK") Then GoTo ZlomZWW
                    i = 1
                    Do Until CInt(vPoz(i, 1)) > m
                        m_Dbl(0) += vPoz(i, 2)
                        m_Dbl(1) += vPoz(i, 3)
                        m_Dbl(2) += vPoz(i, 4)
                        m_Dbl(3) = vPoz(i, 8)
                        i += 1
                        If i.Equals(32) Then Exit Do
                    Loop
                    Dodaj2Tbl(dvP, 7, m_Poz)
                    dvP(m_Poz)("ScrapPurMG") = m_Dbl(0)
                    dvP(m_Poz)("ScrapRecMG") = m_Dbl(1)
                    dvP(m_Poz)("ScrapConMG") = m_Dbl(2)
                    dvP(m_Poz)("ScrapStockMG") = m_Dbl(3)
ZlomZWW:
                    FillStatusBar(0, "Dane o złomie ZWW...")
                    With New Cl4Excel.GetFromXls(m_Path + "ZłomZWW_" + Me.dtpOkres.Value.Year.ToString + ".xls", "Mc" + Okres.Substring(5, 2), "A7")
                        .Data2Array()
                        vPoz = .Dane
                        .Zwolnij()
                    End With
                    If vPoz Is Nothing Then
                        Using New CM
                            MessageBox.Show("Błąd pobrania danych o złomie ZWW", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        GoTo GAZ
                    End If
                    For l = m_Dbl.GetLowerBound(0) To m_Dbl.GetUpperBound(0)
                        m_Dbl(l) = 0
                    Next l
                    SprawdzKontrolne(dtC.Rows(7), aKolumny)
                    If BrakDanychDziennych(vPoz, aKolumny, "ZłomZWW") Then GoTo GAZ
                    i = 1
                    Do Until CInt(vPoz(i, 1)) > m
                        m_Dbl(0) += vPoz(i, 2)
                        m_Dbl(1) += vPoz(i, 3)
                        m_Dbl(2) += vPoz(i, 4)
                        m_Dbl(3) = vPoz(i, 5)
                        i += 1
                        If i.Equals(32) Then Exit Do
                    Loop
                    Dodaj2Tbl(dvP, 4, m_Poz)
                    dvP(m_Poz)("ScrapPurMG") = m_Dbl(0)
                    dvP(m_Poz)("ScrapRecMG") = m_Dbl(1)
                    dvP(m_Poz)("ScrapConMG") = m_Dbl(2)
                    dvP(m_Poz)("ScrapStockMG") = m_Dbl(3)
                    'Zużycie gazu stalownia ZWK i ZWW
GAZ:

                    FillStatusBar(0, "Dane o zużyciu gazu...")
                    With New Cl4Excel.GetFromXls(m_Path + "GAZ_" + Me.dtpOkres.Value.Year.ToString + ".xls", "Mc" + Okres.Substring(5, 2), "A7")
                        .Data2Array()
                        vPoz = .Dane
                        .Zwolnij()
                    End With
                    If vPoz Is Nothing Then
                        Using New CM
                            MessageBox.Show("Błąd pobrania danych o zużyciu gazu", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        GoTo CenaZlomu
                    End If
                    For l = m_Dbl.GetLowerBound(0) To m_Dbl.GetUpperBound(0)
                        m_Dbl(l) = 0
                    Next l
                    SprawdzKontrolne(dtC.Rows(10), aKolumny)
                    If BrakDanychDziennych(vPoz, aKolumny, "GAZ") Then GoTo CenaZlomu
                    i = 1
                    Do Until CInt(vPoz(i, 1)) > m
                        m_Dbl(0) += vPoz(i, 2)
                        m_Dbl(1) += vPoz(i, 3)
                        m_Dbl(2) += vPoz(i, 4)
                        m_Dbl(3) = vPoz(i, 5)
                        i += 1
                        If i.Equals(32) Then Exit Do
                    Loop
                    Dodaj2Tbl(dvP, 4, m_Poz)
                    dvP(m_Poz)("Consum2KWh") = m_Dbl(0)
                    Dodaj2Tbl(dvP, 7, m_Poz)
                    dvP(m_Poz)("Consum2KWh") = m_Dbl(2)
CenaZlomu:
                    FillStatusBar(0, "Dane o cenie złomu...")
                    With New Cl4Excel.GetFromXls(m_Path + "CenaZłomu_" + Me.dtpOkres.Value.Year.ToString + ".xls", "Mc" + Okres.Substring(5, 2), "A7")
                        .Data2Array()
                        vPoz = .Dane
                        .Zwolnij()
                    End With
                    If vPoz Is Nothing Then _
                        Throw New ArgumentException("Błąd pobrania danych o cenie złomu")
                    SprawdzKontrolne(dtC.Rows(0), aKolumny)
                    If BrakDanychDziennych(vPoz, aKolumny, "CenaZłomu") Then Throw New ArgumentException("Błąd pobrania danych o cenie złomu")
                    Dodaj2Tbl(dvP, 4, m_Poz)
                    Try
                        dvP(m_Poz)("ScrapPricePLN") = vPoz(m, 3)
                    Catch ex As Exception
                    End Try
                    Try
                        dvP(m_Poz)("ScrapPriceZakPLN") = vPoz(m, 2)
                    Catch ex As Exception
                    End Try
                    Dodaj2Tbl(dvP, 7, m_Poz)
                    Try
                        dvP(m_Poz)("ScrapPricePLN") = vPoz(m, 4)
                    Catch ex As Exception
                    End Try
                    Try
                        dvP(m_Poz)("ScrapPriceZakPLN") = vPoz(m, 2)
                    Catch ex As Exception
                    End Try
                    tr.Commit()
                Catch ex As Exception
                    tr.Rollback()
                    Throw New ArgumentException(ex.Message)
                End Try
            End Using
            da = DA_TI("tblExpedition", "DataWejscia, KolRaportu")
            da.Update(dtE.GetChanges)
            da = DA_TI("tblProduction", "DataWejscia, KolRaportu")
            da.Update(dtP.GetChanges)
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message & i, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            tr = Nothing
            cmd = Nothing
            da = Nothing
            dt = Nothing
            dtC = Nothing
            dtE = Nothing
            dtP = Nothing
            dvE = Nothing
            dvP = Nothing
            r = Nothing
            oDane = Nothing
            aCtrl = Nothing
            aCtrl2 = Nothing
            aCtrl3 = Nothing
            vPoz = Nothing
            ClearStatus()
        End Try
    End Sub

    Private Sub SprawdzKontrolne(ByVal row As DataRow, ByRef Kolumny As Array)

        If row("KolumnyKontrolne") Is DBNull.Value Then _
            Throw New ArgumentException("Nieokreślone kolumny kontrolne")
        Kolumny = Split(row("KolumnyKontrolne"), ",")
    End Sub

    Private Sub Dodaj2Tbl(ByRef dv As DataView, _
                            ByVal m_Kolumna As Integer, _
                            ByRef m_Poz As Integer)

        Dim r As DataRow
        m_Poz = dv.Find(m_Kolumna)
        If m_Poz.Equals(-1) Then
            r = dv.Table.NewRow
            r("DataWejscia") = Me.dtpOkres.Value.ToShortDateString
            r("KolRaportu") = m_Kolumna
            dv.Table.Rows.Add(r)
            m_Poz = dv.Find(m_Kolumna)
        End If
    End Sub

    Private Sub Wizualizacja() Handles tsmWizualizacja.Click

        Dim i, j As Integer
        Dim da As New OleDbDataAdapter
        Dim dt As New DataTable
        Dim aExp(,) As Object
        Dim aProd(0, 0) As Object
        Dim aWIP(,) As Object
        Dim m_WB As String = Path2Xls + XlsName
        Dim m_Blad As String = ""
        Dim _cns As New OleDbConnection
        Const CO_ACTION As String = "Wizualizacja"
        Try
            If Not SprawdzDate() Then Exit Try
            ' pobieranie WIP
            Dim m_Ado As New ClADO.ADOGet
            Try
                With m_Ado
                    .Baza = "dbCHO_" + Okres.Substring(0, 4) + ".mdb"
                    .Folder = My.Settings.CHO_Sciezka
                    .Haslo = My.Settings.Myk
                    If Not .OpenData(True) Then _
                        Throw New ArgumentException("Brak połączenia z plikiem bazy danych [" + .Baza + "]")
                    _cns = .CN
                End With
            Catch ex As Exception
                Throw New ArgumentException(ex.Message)
            Finally
                m_Ado = Nothing
            End Try
            ' 2013-10-09 pobieranie WIP do dbCHO
            Using New CM
                If MessageBox.Show("Pobierasz bezpośrednio z Excel-a?", "Dane WIP", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) _
                    .Equals(Windows.Forms.DialogResult.Yes) Then DaneWIP(_cns)
            End Using
            FillStatusBar(, CO_ACTION + "...")
            da = New OleDbDataAdapter("SELECT Instalacja, SUM(AktualnyZapas) FROM tblWIP WHERE Okres = '" + Okres.Substring(0, 7) + "' GROUP BY Instalacja ", _cns)
            da.Fill(dt)
            _cns = Nothing
            ReDim aWIP(dt.Rows.Count, dt.Columns.Count - 1)
            For i = 0 To dt.Rows.Count - 1
                For j = 0 To dt.Columns.Count - 1
                    If j.Equals(1) Then
                        aWIP(i, j) = CP.Str2Dbl(dt.Rows(i)(j).ToString)
                    Else
                        aWIP(i, j) = dt.Rows(i)(j)
                    End If
                Next j
            Next i
            dt = New DataTable
            da = DA_TI("tblProduction", "KolRaportu", "DataWejscia = #" + Okres + "#")
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then
                Using New CM
                    If MessageBox.Show("Wizualizacja bez dziennej produkcji?", Me.tsmWizualizacja.Text.Replace("&", ""), MessageBoxButtons.YesNo, MessageBoxIcon.Question).Equals(Windows.Forms.DialogResult.No) Then _
                                Throw New ArgumentException()
                End Using
            End If
            dt.Clear()
            With Me.dtpOkres.Value
                da = DA_TI("tblProduction", "KolRaportu, DataWejscia DESC", "DataWejscia <= #" + Okres _
                           + "# AND YEAR(DataWejscia) = " + .Year.ToString + " AND MONTH(DataWejscia) = " + .Month.ToString)
            End With
            da.Fill(dt)
            ReDim aProd(dt.Rows.Count, dt.Columns.Count - 1)
            For j = 0 To dt.Columns.Count - 1
                aProd(0, j) = dt.Columns(j).ColumnName
            Next j
            For i = 1 To dt.Rows.Count
                For j = 0 To dt.Columns.Count - 1
                    If j > 1 Then
                        aProd(i, j) = CP.Str2Dbl(dt.Rows(i - 1)(j).ToString)
                    Else
                        aProd(i, j) = dt.Rows(i - 1)(j)
                    End If
                Next j
            Next i
            dt = New DataTable
            With Me.dtpOkres.Value
                da = DA_TI("tblExpedition", "KolRaportu, DataWejscia DESC", "DataWejscia <= #" + Okres _
                           + "# AND YEAR(DataWejscia) = " + .Year.ToString + " AND MONTH(DataWejscia) = " + .Month.ToString)
            End With
            da.Fill(dt)
            ReDim aExp(dt.Rows.Count, dt.Columns.Count - 1)
            For j = 0 To dt.Columns.Count - 1
                aExp(0, j) = dt.Columns(j).ColumnName
            Next j
            For i = 1 To dt.Rows.Count
                For j = 0 To dt.Columns.Count - 1
                    If j > 1 Then
                        aExp(i, j) = CP.Str2Dbl(dt.Rows(i - 1)(j).ToString)
                    Else
                        aExp(i, j) = dt.Rows(i - 1)(j)
                    End If
                Next j
            Next i
            ' Otworzenie pliku programowego
            With New Cl4Excel.Excel(m_WB, , , "m")
                With .oApp
                    .Run(CO_ACTION, Me.dtpOkres.Value, aProd, aExp, aWIP)
                    m_Blad = .Range("Blad").Value2
                End With
                If Not String.IsNullOrEmpty(m_Blad) Then .Zwolnij() : Throw New ArgumentException(m_Blad)
                .oBook.Save()
                HideScreen()
                .Show()
            End With
        Catch ex As Exception
            ClearStatus()
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION + " " + Okres, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            dt = Nothing
            da = Nothing
            aExp = Nothing
            aProd = Nothing
            aWIP = Nothing
            HideScreen(False)
        End Try
    End Sub

    Private Sub DaneWIP(ByVal Conn As OleDbConnection)

        Dim strPath As String = My.Settings.ToWIP
        Dim m_Ext As String = ".xl*"
        Dim strGrMater As String = ""
        Dim strGrKalk As String = ""
        Dim strMaterial As String = ""
        Dim strKlient As String = ""
        Dim strWydzial As String = ""
        Dim da As OleDbDataAdapter
        Dim dr As OleDbDataReader
        Dim dt As New DataTable
        Dim cmd As New OleDbCommand
        Dim tr As OleDbTransaction = Nothing
        Dim i, iR, iPl As Integer
        Dim K() As Integer = Nothing
        Dim m_Waga, m_Check, m_Waga3, m_Waga4, m_Waga5 As Double
        Dim m_Kwota As Double
        Dim m_Key As String
        Dim m_Klient, m_Status As String
        Dim m_FI() As FileInfo
        Dim blnEx As Boolean
        Dim aDane As Array
        Const CO_ACTION As String = "Pobieranie danych WIP "
        ' Sprawdź, czy są pliki
        strPath = CP.GetSourcePath(strPath + "BWD_WIP_*", m_Ext, CO_ACTION)
        If String.IsNullOrEmpty(strPath) Then Return
        With My.Settings
            .ToWIP = strPath
            .Save()
        End With
        m_FI = New DirectoryInfo(strPath).GetFiles("BWD_WIP_*" + m_Ext)
        Using cn As New OleDbConnection(Conn.ConnectionString)
            cn.Open()
            tr = cn.BeginTransaction
            cmd.Connection = cn
            cmd.Transaction = tr
            Try
                For iPl = 0 To m_FI.GetUpperBound(0)
                    With New Cl4Excel.GetFromXls(m_FI(iPl).FullName, "BWD", "J5:J6")
                        .Data2Array()
                        aDane = .Dane
                        If aDane Is Nothing Then .Zwolnij() : Throw New ArgumentException
                        If aDane.GetValue(2, 1).ToString.Substring(0, 2) <> Okres.Substring(5, 2) _
                                OrElse aDane.GetValue(2, 1).ToString.Substring(3, 4) <> Okres.Substring(0, 4) Then
                            .Zwolnij() : Throw New ArgumentException("Wybrany okres nie odpowiada okresowi danych BWD")
                        End If
                        .Zakres = "F15:X"
                        .Data2Array()
                        aDane = .Dane
                        .Zwolnij()
                    End With
                    If aDane Is Nothing Then _
                        Throw New ArgumentException("Plik [" + m_FI(iPl).Name + "] nie spełnia oczekiwań")
                    ' sprawdzenie układu kolumn 
                    If Not CP.SourceLayoutControl(aDane, "BWD_WIP", Me.cns, K) Then Exit Try
                    With Me
                        .tsslMain.Text = "Przetwarzanie danych BWD..."
                        With .tspbMain
                            .Minimum = 0
                            .Maximum = aDane.GetLength(0)
                        End With
                    End With
                    If iPl.Equals(0) Then
                        With cmd
                            .CommandText = "DELETE FROM tblWIP WHERE Okres = '" + Okres.Substring(0, 7) + "'"
                            .ExecuteNonQuery()
                        End With
                    End If
                    i = 2
                    Do Until i > aDane.GetLength(0)
                        If aDane(i, 5) Is Nothing Then
                            i += 1
                            GoTo nastepny
                        End If
                        strWydzial = "  "
                        With aDane(i, 5).ToString
                            If String.IsNullOrEmpty(.Trim) Then
                                i += 1
                                GoTo nastepny
                            ElseIf .Trim.Equals("#") Then
                            ElseIf .StartsWith("J7") Then
                                strWydzial = "MT"
                            ElseIf .StartsWith("J43") Then
                                strWydzial = "MS"
                            ElseIf .StartsWith("J48") Then
                                strWydzial = "PS"
                            End If
                        End With
                        ' kontrola i zapis grup materiałowych
                        If Not strGrMater.Contains(aDane(i, 3).ToString) Then
                            With cmd
                                .CommandText = "SELECT TOP 1 * FROM tblGrupyMat WHERE ID = '" + aDane.GetValue(i, 3).ToString + "'"
                                dr = .ExecuteReader
                                blnEx = dr.HasRows
                                dr.Close()
                                If Not blnEx Then
                                    .CommandText = "INSERT INTO tblGrupyMat (ID, Wydzial, GrMaterPL) " _
                                        + " VALUES ('" + aDane(i, 3).ToString + "', '" + strWydzial + "', '" + aDane.GetValue(i, 4).ToString + "')"
                                    .ExecuteNonQuery()
                                End If
                            End With
                            strGrMater &= "#" + aDane(i, 3).ToString
                        End If
                        ' kontrola i zapis grup kalkulacyjnych
                        If Not strGrKalk.Contains(aDane(i, 5).ToString) Then
                            With cmd
                                .CommandText = "SELECT TOP 1 * FROM tblGrupyKalk WHERE ID = '" + aDane.GetValue(i, 5).ToString + "'"
                                dr = .ExecuteReader
                                blnEx = dr.HasRows
                                dr.Close()
                                If Not blnEx Then
                                    .CommandText = "INSERT INTO tblGrupyKalk (ID, GrMater, Wydzial, GrKalkPL) " _
                                        + " VALUES ('" + aDane.GetValue(i, 5).ToString + "', '" + aDane.GetValue(i, 3).ToString _
                                        + "', '" + strWydzial + "', '" + aDane.GetValue(i, 6).ToString + "')"
                                    .ExecuteNonQuery()
                                End If
                            End With
                            strGrKalk &= "#" + aDane.GetValue(i, 5).ToString
                        End If
                        ' kontrola i zapis materiałów
                        If strMaterial.IndexOf(aDane.GetValue(i, 7).ToString) = -1 Then
                            With cmd
                                .CommandText = "SELECT TOP 1 * FROM tblMaterialy WHERE ID = '" + aDane.GetValue(i, 7).ToString + "'"
                                dr = .ExecuteReader
                                blnEx = dr.HasRows
                                dr.Close()
                                If Not blnEx Then
                                    .CommandText = "INSERT INTO tblMaterialy (ID, GrMaterID, GrKalkID, MaterialPL) " _
                                        + " VALUES ('" + aDane.GetValue(i, 7).ToString + "', '" + aDane.GetValue(i, 3).ToString + "', '" + aDane.GetValue(i, 5).ToString + "', '" + aDane.GetValue(i, 8).ToString + "')"
                                    .ExecuteNonQuery()
                                End If
                            End With
                            strMaterial &= "#" + aDane.GetValue(i, 7).ToString
                        End If
                        ' kontrola i zapis klientów
                        m_Klient = aDane.GetValue(i, 9).ToString
                        If m_Klient.Equals("#") Then m_Klient = "999"
                        If strKlient.IndexOf(m_Klient).Equals(-1) Then
                            With cmd
                                .CommandText = "SELECT TOP 1 * FROM tblKlienci WHERE ID = " + m_Klient
                                dr = .ExecuteReader
                                blnEx = dr.HasRows
                                dr.Close()
                                If Not blnEx Then
                                    .CommandText = "INSERT INTO tblKlienci (ID, Nazwa) " _
                                        + " VALUES (" + m_Klient + ", '" + aDane.GetValue(i, 10).ToString.Replace("'", "") + "')"
                                    .ExecuteNonQuery()
                                End If
                            End With
                            strKlient &= "#" + m_Klient
                        End If
                        m_Key = aDane.GetValue(i, 11).ToString
                        m_Kwota = 0
                        m_Waga = 0
                        iR = i
                        ' sprawdzenie zleceń
                        While i <= aDane.GetLength(0) AndAlso aDane.GetValue(i, 11) IsNot Nothing AndAlso aDane.GetValue(i, 11).ToString = m_Key
                            With aDane
                                Try
                                    m_Check = CType(.GetValue(i, 17), Double)
                                Catch ex As Exception
                                    m_Check = 0
                                End Try
                                If m_Check > 0 Then
                                    m_Waga += m_Check
                                End If
                                m_Kwota += CType(.GetValue(i, 18), Double)
                            End With
                            i += 1
                        End While
                        If Math.Round(m_Waga, 1).Equals(0) Then
                            If Not Math.Round(m_Kwota, 1).Equals(0) Then
                                ' zapis zlecenia bez partii
                                With aDane
                                    m_Status = .GetValue(iR, 12).ToString
                                    If m_Status.Length > 4 Then m_Status = "BŁĄD"
                                    cmd.CommandText = "INSERT INTO tblWIP (Okres, NrZlecenia, Partia, Status, Material, IDKlienta, GrMater, GrKalk, Kwota, Instalacja) " _
                                        + " VALUES ('" + Okres.Substring(0, 7) + "', '" + m_Key + "','', '" + m_Status + "', '" _
                                        + .GetValue(iR, 7).ToString + "', " + m_Klient + ", '" + _
                                        .GetValue(iR, 3).ToString + "', '" + .GetValue(iR, 5).ToString _
                                        + "', " + CP.Dbl2Str(m_Kwota) + ", '"
                                    If .GetValue(iR, 1).ToString.Equals("#") Then
                                        cmd.CommandText += "JSU01"
                                    Else
                                        cmd.CommandText += .GetValue(iR, 1).ToString
                                    End If
                                    cmd.CommandText += "')"
                                    Try
                                        cmd.ExecuteNonQuery()
                                    Catch ex As Exception
                                        Throw New ArgumentException("Błąd zapisu klucza [" + .GetValue(iR, 1).ToString + " " + m_Key + " " + .GetValue(iR, 13).ToString + "]")
                                    End Try
                                End With
                            End If
                        Else
                            i = iR
                            While (i <= aDane.GetLength(0) AndAlso Not aDane.GetValue(i, 11) Is Nothing) AndAlso aDane.GetValue(i, 11).ToString = m_Key
                                With aDane
                                    m_Check = CType(.GetValue(i, 17), Double) ' waga końcowa
                                    If m_Check > 0 Then
                                        m_Waga3 = CType(.GetValue(i, 15), Double)
                                        m_Waga4 = CType(.GetValue(i, 16), Double) ' waga
                                        m_Waga5 = CType(.GetValue(i, 19), Double) ' waga
                                        m_Status = .GetValue(iR, 12).ToString
                                        If m_Status.Length > 4 Then m_Status = "BŁĄD"
                                        cmd.CommandText = "INSERT INTO tblWIP (Okres, NrZlecenia, Partia, Status, Material, IDKlienta, GrMater, GrKalk, Waga, Kwota, WagaOdkuwki, WagaKoncowa, AktualnyZapas, Instalacja) " _
                                                + " VALUES ('" + Okres.Substring(0, 7) + "', '" + m_Key + "', '" + .GetValue(i, 13).ToString + "', '" + m_Status + "', '" _
                                                + .GetValue(iR, 7).ToString + "', " + m_Klient + ", '" + .GetValue(iR, 3).ToString + "', '" + .GetValue(iR, 5).ToString _
                                                + "', " + CP.Dbl2Str(m_Waga4 / 1000) + ", " + CP.Dbl2Str(Math.Round(m_Kwota * m_Check / m_Waga, 2)) + ", " + CP.Dbl2Str(m_Waga3 / 1000) + ", " + CP.Dbl2Str(m_Check / 1000) + ", " + CP.Dbl2Str(m_Waga5 / 1000) + ", '"
                                        If .GetValue(i, 1).ToString.Equals("#") Then
                                            cmd.CommandText += "JSU01')"
                                        Else
                                            cmd.CommandText += .GetValue(i, 1).ToString + "')"
                                        End If
                                        Try
                                            cmd.ExecuteNonQuery()
                                        Catch ex As Exception
                                            Throw New ArgumentException("Błąd zapisu klucza [" + .GetValue(iR, 1).ToString + " " + m_Key + " " + .GetValue(iR, 13).ToString + "]")
                                        End Try
                                    End If
                                End With
                                i += 1
                            End While
                        End If
Nastepny:
                        With Me.tspbMain
                            If i <= .Maximum AndAlso (i Mod 5).Equals(0) Then _
                                .Value = i : Application.DoEvents()
                        End With
                    Loop
                Next iPl
                tr.Commit()
                Using New CM
                    MessageBox.Show(CO_ACTION + " zakończone pomyślnie", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Using
            Catch ex As Exception
                Try
                    tr.Rollback()
                Catch
                End Try
                Using New CM
                    MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Using
            End Try
        End Using
        m_FI = Nothing
        da = Nothing
        dt = Nothing
        dr = Nothing
        cmd = Nothing
        tr = Nothing
    End Sub

    Private Sub Miesieczne() Handles tsmMiesieczne.Click

        Dim m_WB As String = Path2Xls + XlsName
        Dim m_Blad As String = ""
        Const CO_ACTION As String = "Miesieczne"
        Try
            If Not SprawdzDate() Then Exit Try
            FillStatusBar(, CO_ACTION + "...")
            ' Otworzenie pliku programowego
            With New Cl4Excel.Excel(m_WB, , , "m")
                With .oApp
                    .DisplayAlerts = False
                    .Run(CO_ACTION, Me.dtpOkres.Value)
                    m_Blad = .Range("Blad").Value2
                End With
                If Not String.IsNullOrEmpty(m_Blad) Then .Zwolnij() : Throw New ArgumentException(m_Blad)
                .oBook.Save()
                .Zwolnij()
            End With
            Using New CM
                MessageBox.Show("Operacja zakończona pomyślnie", CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Using
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION + " " + Okres, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            ClearStatus()
        End Try
    End Sub

    Private Sub Rozeslij() Handles tsmRozeslij.Click

        Dim m_WB As String = Path2Xls + XlsName
        Dim m_Blad As String = ""
        Dim dt As New DataTable
        Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT Swieta FROM tblSwieta WHERE Rok = '" + Okres.Substring(0, 4) + "'", Me.cns)
        Dim cMail As New ClSendEmail.SendEmail
        Dim m_Path, m_Plik As String
        Dim m_Rng As Object
        Const CO_ACTION As String = "Rozsyłanie poczty"
        Try
            If Not SprawdzDate() Then Exit Try
            FillStatusBar(, CO_ACTION + "...")
            da.Fill(dt)
            If dt.Rows.Count.Equals(0) Then Throw New ArgumentException("Uzupełnij święta ruchome (Pomocne->Dane stałe->święta)")
            With New Cl4Excel.Excel(m_WB, , , "m")
                With .oApp
                    .ScreenUpdating = False
                    m_Rng = .Range("DataPrzetwarzania").Value2
                End With
                If Not CDate(m_Rng).Equals(Me.dtpOkres.Value) Then _
                    .Zwolnij() : Throw New ArgumentException("Dane można rozesłać po uprzednim uruchomieniu opcji [Wizualizacja] dla wybranego okresu.")
                With .oApp
                    .Run("Rozeslij", Me.dtpOkres.Value, dt.Rows(0)("Swieta").ToString)
                    m_Blad = .Range("Blad").Value2
                    m_Rng = .Range("IleDni").Value2
                End With
                If Not String.IsNullOrEmpty(m_Blad) Then .Zwolnij() : Throw New ArgumentException(m_Blad)
                m_Path = .oBook.Path + "\Export\"
                m_Plik = "CelsaHO_" + Okres + ".xls"
                With .oBook
                    If .FullName.EndsWith("m") Then m_Plik += "x"
                    .Save()
                End With
                .Zwolnij()
            End With
            ' Rozesłanie poczty
            With cMail
                .UsunHtml = True
                .Tytul = "Monitoring dzienny CHO " + Okres
                .Odbiorca = My.Settings.ListaDystrybucji
                .Zalacznik = m_Path + m_Plik
                .Tresc = My.Settings.Body
                .Wyslij()
            End With
            Using New CM
                If CInt(m_Rng) > 1 _
                    AndAlso Not String.IsNullOrEmpty(My.Settings.ListaPoniedzialkowa) _
                    AndAlso MessageBox.Show("Wysłać [Poniedziałkowym]?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(vbYes) Then GoTo Poniedzialkowi
            End Using
            Exit Try
Poniedzialkowi:
            With cMail
                .UsunHtml = True
                .Tytul = "Daily monitor of Celsa Huta Ostrowiec " + Okres
                .Odbiorca = My.Settings.ListaPoniedzialkowa
                .Zalacznik = m_Path + m_Plik
                .Tresc = My.Settings.Body
                .Wyslij()
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION + " " + Okres, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
        Finally
            ClearStatus()
        End Try
    End Sub
    Private Sub AktualizujScrapPrices(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmScrapPrices.Click

        Dim SCPrice_Workbook As String
        Dim xls_App As XL.Application = Nothing
        Dim xls_Workbooks As XL.Workbooks = Nothing
        Dim xls_WB As XL.Workbook = Nothing

        SCPrice_Workbook = "W:\Scrap Company\ZAKUP CHO ZŁOM\DAILY-final stock of scrap_" + (Year(dtpOkres.Value).ToString()) + ".xls"
        tsslMain.Text = "Aktualizacja Arkusza DAILY-final stock of scrap_" + (Year(dtpOkres.Value).ToString()) + ".xls"

        Try

            xls_App = New XL.Application
            'xls_App.Visible = True
            xls_Workbooks = xls_App.Workbooks
            xls_WB = xls_Workbooks.Open(SCPrice_Workbook, True)
            xls_WB.Save()
            xls_WB.Close()
            

        Catch ex As Exception
            MessageBox.Show("Brak pliku: " + SCPrice_Workbook)
        Finally
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xls_WB)
            xls_WB = Nothing
            xls_App.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xls_Workbooks)
            xls_Workbooks = Nothing
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xls_App)
            xls_App = Nothing
        End Try
        tsslMain.Text = "Gotowe"
        Application.DoEvents()

    End Sub

    Private Function BrakDanychDziennych(ByVal aTablica As Array, _
                                         ByVal aKolumny As Array, _
                                         Optional ByVal Gdzie As String = "", _
                                         Optional ByVal ForEmail As Boolean = False) As Boolean

        Dim i, j, k As Integer
        Dim m_Dbl As Double
        Dim m_Err As Boolean = True
        Try
            i = Me.dtpOkres.Value.Day
            For k = aKolumny.GetLowerBound(0) To aKolumny.GetUpperBound(0)
                Try
                    j = CInt(aKolumny(k))
                    m_Dbl = aTablica(i, j)
                    If Not m_Dbl.Equals(0) Then
                        m_Err = False
                        Exit Function
                    End If
                Catch ex As Exception
                End Try
            Next k
        Catch ex As Exception
        End Try
        Using New CM
            If m_Err AndAlso Not ForEmail Then _
                MessageBox.Show("Brak danych za " + Me.dtpOkres.Value.ToShortDateString, Gdzie, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Using
        Return m_Err
    End Function

#End Region ' Menu dzienne

#Region "Menu pomocne"

    Private Sub KontrolaKompletnosci_Click() Handles tsmKontrolaKompletnosci.Click

        With My.Settings
            .KontrolaKompletnosci = Not .KontrolaKompletnosci
        End With
        With Me.TimerOfDepData
            .Enabled = My.Settings.KontrolaKompletnosci
            If .Enabled Then
                .Start()
            Else
                .Stop()
            End If
            Using New CM
                MessageBox.Show("Cykliczne sprawdzanie danych wydziałowych w" + IIf(Not .Enabled, "y", "") + "łączone")
            End Using
        End With
    End Sub

    Private Sub TimerOfDepData_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerOfDepData.Tick

        Dim da As OleDbDataAdapter = DA_TI("tblKontrola", "ID")
        Dim dt As New DataTable
        Dim m_Mc As Integer = Me.dtpOkres.Value.Month
        Dim m_Path As String = "W:\CHO Dzienny\Dane_" + Me.dtpOkres.Value.Year.ToString + "\"
        Dim Kompletne As Boolean = True
        Dim m_Ark As String = "Mc" + Okres.Substring(5, 2)
        Dim m_WB As String
        Dim aKolumny As Array
        Const CO_ACTION As String = "Kontrola danych wydziałowych"
        Try
            If Not SprawdzDate() Then Throw New ArgumentException
            da.Fill(dt)
            For Each r As DataRow In dt.Rows
                m_WB = r("Kontrola").ToString.Replace("_rrrr", "_" + Me.dtpOkres.Value.Year.ToString) + ".xls"
                If r("KolumnyKontrolne") Is DBNull.Value Then _
                    Throw New ArgumentException(m_WB + ": Nieokreślone kolumny kontrolne")
                aKolumny = Split(r("KolumnyKontrolne").ToString, ",")
                If Not My.Computer.FileSystem.FileExists(m_Path + m_WB) Then GoTo Nastepny
                Dim aDane As Array
                With New Cl4Excel.GetFromXls(m_Path + m_WB, m_Ark, "A7")
                    .Data2Array()
                    aDane = .Dane
                    .Zwolnij()
                End With
                If Not BrakDanychDziennych(aDane, aKolumny, m_WB, True) Then GoTo Nastepny
                Kompletne = False
                If MessageBox.Show(r("Email").ToString + " - wysyłasz?", CO_ACTION, MessageBoxButtons.YesNo, _
                                       MessageBoxIcon.Question).Equals(DialogResult.Yes) Then GoTo MailaSlij

                If MessageBox.Show("Rezygnujesz z wysyłania ponagleń?", CO_ACTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question, _
                                        MessageBoxDefaultButton.Button2).Equals(DialogResult.Yes) Then
                    Kompletne = True : Exit For
                End If
MailaSlij:
                With New ClSendEmail.SendEmail
                    .UsunHtml = True
                    .Odbiorca = r("Email").ToString
                    .Tytul = "Data of Daily Monitoring"
                    .Tresc = "Witam, " + vbCrLf _
                                + "Bardzo proszę pilnie uzupełnić dane do monitoringu dziennego" + vbCrLf _
                                + "w skoroszycie " + m_Path + m_WB + vbCrLf _
                                + "Pozdrawiam"
                    .Wyslij()
                End With
Nastepny:
            Next r
            If Kompletne Then
                With Me.TimerOfDepData
                    .Stop()
                    .Enabled = False
                End With
            End If
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, CO_ACTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            da = Nothing
            dt = Nothing
        End Try
    End Sub

    Private Sub ZmianaUstawienParametrow(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tscmbDostepy.SelectedIndexChanged

        Dim m_Tytul As String = "Zmiana ustawienia " + Me.tscmbDostepy.Text
        Dim m_Txt As String = ""
        Try
            With My.Settings
                Select Case Me.tscmbDostepy.SelectedIndex
                    Case 0
                        m_Txt = .Podpis
                    Case 1
                        m_Txt = .Sciezka
                    Case 2
                        m_Txt = .ListaDystrybucji
                    Case 3
                        m_Txt = .ListaPoniedzialkowa
                    Case 4
                        m_Txt = .CHO_Sciezka
                    Case Else
                        Exit Try
                End Select
            End With
            If Me.tscmbDostepy.SelectedIndex.Equals(2) OrElse Me.tscmbDostepy.SelectedIndex.Equals(3) Then
                m_Txt = InputBox(m_Tytul + vbCrLf + "(użyj średnika jako separatora)", "Parametry", m_Txt, CP.PositionX, CP.PositionY)
                m_Txt = m_Txt.Trim
                If Not String.IsNullOrEmpty(m_Txt) Then
                    With My.Settings
                        If Me.tscmbDostepy.SelectedIndex.Equals(2) Then
                            .ListaDystrybucji = m_Txt.ToLower
                        Else
                            .ListaPoniedzialkowa = m_Txt.ToLower
                        End If
                        .Save()
                    End With
                Else
                    Using New CM
                        If MessageBox.Show("Chcesz usunąć całą zawartość?", "Zmiana ustawień", MessageBoxButtons.YesNo, MessageBoxIcon.Question, _
                                                               MessageBoxDefaultButton.Button2).Equals(Windows.Forms.DialogResult.Yes) Then
                            With My.Settings
                                If Me.tscmbDostepy.SelectedIndex.Equals(2) Then
                                    .ListaDystrybucji = ""
                                Else
                                    .ListaPoniedzialkowa = ""
                                End If
                                .Save()
                            End With
                        End If
                    End Using
                End If
                Exit Try
            End If
            Dim fbd As New FolderBrowserDialog
            Try
                With fbd
                    .Description = m_Tytul
                    .SelectedPath = m_Txt
                    .ShowNewFolderButton = False
                    If .ShowDialog().Equals(Windows.Forms.DialogResult.OK) Then
                        m_Txt = .SelectedPath + Path.DirectorySeparatorChar
                    Else
                        Exit Try
                    End If
                End With
                With My.Settings
                    Select Case Me.tscmbDostepy.SelectedIndex
                        Case 0
                            .Podpis = m_Txt
                        Case 1
                            .Sciezka = m_Txt
                        Case 4
                            .CHO_Sciezka = m_Txt
                    End Select
                    .Save()
                End With
            Catch ex As Exception
                Using New CM
                    MessageBox.Show(ex.Message, "Zmiana ścieżki dostępu", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
            Finally
                fbd = Nothing
            End Try
        Catch ex As Exception
        End Try
        Me.tscmbDostepy.SelectedIndex = -1
    End Sub

    Private Sub DBKompaktuj() Handles tsmKompaktuj.Click

        Dim c As New ClADO.CompareMdb
        Dim m_FI() As FileInfo
        Dim m_Path = "C:\CHO Monitoring\Archiwum\"
        Dim m_Date As Date
        Dim i As Integer
        With Me
            .Cursor = Cursors.WaitCursor
            .tsslMain.Text = "Kompaktowanie bazy danych"
        End With
        Application.DoEvents()
        With My.Settings
            c.CompactMdb(Path2Db:=.Sciezka, _
                         DbFile:=.Baza, _
                         Dostep:=.Myk, _
                         Path2Arch:=m_Path)
        End With
        m_FI = New DirectoryInfo(m_Path).GetFiles("dbCHOMonitor_*.mdb")
        If m_FI.Length > 2 Then
            m_Date = Now.AddDays(-7)
            Using New CM
                If MessageBox.Show("Usunąć kopie DB sprzed tygodnia?", Me.tsslMain.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    For i = 0 To m_FI.GetUpperBound(0)
                        If CP.CompareDates(True, m_Date, CDate(m_FI(i).Name.Substring(13, 10))) Then
                            My.Computer.FileSystem.DeleteFile(m_FI(i).FullName)
                        End If
                    Next i
                End If
            End Using
        End If
        ClearStatus()
    End Sub

    Private Sub tsmSzerokoscOkresu_Click() Handles tsmSzerokoscOkresu.Click

        Dim m_Width As Integer
        Dim m_New As String
        With Me
            m_Width = .dtpOkres.Width
            m_New = InputBox("Wpisz liczbę całkowitą - aktualnie:", "Zmiana szerokości okna okresu", m_Width.ToString, CP.PositionX, CP.PositionY)
            If m_New = "" Then Exit Sub
            With .dtpOkres
                .Width = CInt(m_New)
                .Location = New Point(Me.Width - .Width - 15, 0)
                My.Settings.OkresSize = .Size
            End With
        End With
    End Sub

#End Region 'Menu pomocne

#Region "Metody pomocnicze"

    Private Sub ZmienJezyk() Handles tsmZmienJezyk.Click

        Dim m_WB As String = Path2Xls + XlsName
        Dim m_Blad As String
        Try
            With My.Settings
                If .Language.Equals(1) Then
                    .Language = 2
                    Using New CM
                        MessageBox.Show("Active language is English", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Using
                Else
                    .Language = 1
                    Using New CM
                        MessageBox.Show("Aktywnym językiem jest polski", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Using
                End If
            End With
            FillStatusBar(, "Zmiana języka...")
            With New Cl4Excel.Excel(m_WB)
                With .oApp
                    .Run("ZmienJezyk", My.Settings.Language)
                    m_Blad = CStr(.Range("Blad").Value2)
                End With
                If Not String.IsNullOrEmpty(m_Blad) Then .Zwolnij() : Throw New ArgumentException(m_Blad)
                .oBook.Save()
                .Zwolnij()
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Zmiana języka", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            ClearStatus()
        End Try
    End Sub

    Private Function SprawdzDate() As Boolean

        If Me.dtpOkres.Value > Now Then
            Using New CM
                MessageBox.Show("Nie możesz działać w przyszłości", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Using
            Return False
        End If
        Return True
    End Function

    Private Sub PokazTabele(ByVal sender As System.Object, ByVal e As System.EventArgs) _
                                    Handles tsmtblExpedition.Click, _
                                            tsmtblKontrola.Click, _
                                            tsmtblProduction.Click, _
                                            tsmtblSources.Click, _
                                            tsmtblSwieta.Click, _
                                            tsmtblRabaty.Click



        Dim ctrl As ToolStripMenuItem = DirectCast(sender, ToolStripMenuItem)
        Dim m_Tbl As String = ctrl.Name.Substring(3)
        Dim m_Dane As New ClDane.frmDane
        Dim da As New OleDbDataAdapter
        Dim dt As New DataTable
        Dim m_Filtr As String = ""
        Dim i As Integer
        Dim dtF As New DataTable
        Try
            With dtF.Columns
                .Add("Column")
                .Add("Format")
                .Add("Alignment")
            End With
            WybranaTabela = m_Tbl
            With Me
                .tsslMain.Text = "Odczyt danych " + WybranaTabela + "..."
                .Cursor = Cursors.WaitCursor
            End With
            Application.DoEvents()
            With m_Dane
                Select Case WybranaTabela
                    Case "tblExpedition"
                        m_Filtr = "DataWejscia = #" + Okres + "#"
                        da = DA_TI(WybranaTabela, "DataWejscia, KolRaportu")
                        .Size = My.Settings.scrExpedition
                        For i = 2 To 18
                            Select Case i
                                Case 2, 4, 8, 10, 12
                                    dtF.Rows.Add(New Object() {i, "# ### ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                                Case Else
                                    dtF.Rows.Add(New Object() {i, "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            End Select
                        Next i
                        .ColToFroze = 1
                        .FreshFromXls = True
                    Case "tblKontrola"
                        da = DA_TI(WybranaTabela, "ID")
                        .Size = My.Settings.scrKontrola
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblProduction"
                        m_Filtr = "DataWejscia = #" + Okres + "#"
                        da = DA_TI(WybranaTabela, "DataWejscia, KolRaportu")
                        .Size = My.Settings.scrProduction
                        For i = 2 To 25
                            Select Case i
                                Case 2, 3, 4, 8, 16, 17, 18, 19, 20, 23, 24, 25
                                    dtF.Rows.Add(New Object() {i, "# ### ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                                Case 10 To 14
                                    dtF.Rows.Add(New Object() {i, "# ### ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                                Case 16 To 20
                                    dtF.Rows.Add(New Object() {i, "# ### ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                                Case 23 To 25
                                    dtF.Rows.Add(New Object() {i, "# ### ##0.000", CType(DataGridViewContentAlignment.TopRight, Integer)})
                                Case 6 To 7
                                    dtF.Rows.Add(New Object() {i, "# ### ##0", CType(DataGridViewContentAlignment.TopRight, Integer)})
                                Case 9
                                    dtF.Rows.Add(New Object() {i, "# ### ##0.0", CType(DataGridViewContentAlignment.TopRight, Integer)})
                                Case Else
                                    dtF.Rows.Add(New Object() {i, "# ### ##0.00", CType(DataGridViewContentAlignment.TopRight, Integer)})
                            End Select
                        Next i
                        .ColToFroze = 1
                        .FreshFromXls = True
                    Case "tblSources"
                        da = DA_TI("tblSources", "Source, NrKol")
                        .Size = My.Settings.scrSources
                        .ColToFroze = 0
                        .FreshFromXls = True
                    Case "tblSwieta"
                        da = DA_TI(WybranaTabela, "Rok", )
                        .Size = My.Settings.scrSwieta
                    Case "tblRabaty"
                        da = DA_TI(WybranaTabela)
                        .Size = My.Settings.scrSwieta
                End Select
            End With
            If Not String.IsNullOrEmpty(m_Filtr) Then
                With da.SelectCommand
                    i = .CommandText.IndexOf("ORDER BY")
                    If i > 0 Then
                        .CommandText = .CommandText.Substring(0, i) + " WHERE " + m_Filtr + " " + .CommandText.Substring(i - 1)
                    Else
                        .CommandText += " WHERE " + m_Filtr
                    End If
                End With
            End If
            With m_Dane
                .NazwaTabeli = WybranaTabela
                .tsbCopy.Enabled = False
                .Okres = Me.dtpOkres.Value.ToShortDateString
                .Polaczenie = Me.cns.ConnectionString
                .PolecenieSelect = da.SelectCommand.CommandText
                .Formatowanie = dtF
                HideScreen()
                .ShowDialog()
                Select Case WybranaTabela
                    Case "tblExpedition"
                        My.Settings.scrExpedition = .Size
                    Case "tblKontrola"
                        My.Settings.scrKontrola = .Size
                    Case "tblProduction"
                        My.Settings.scrProduction = .Size
                    Case "tblSources"
                        My.Settings.scrSources = .Size
                    Case "tblSwieta"
                        My.Settings.scrSwieta = .Size
                End Select
                If .Zapisane.Equals(1) Then
                    dt = New DataTable
                    dt = .Dane.GetChanges
                    If dt Is Nothing OrElse dt.Rows.Count.Equals(0) Then Exit Try
                    da.Update(dt)
                End If
            End With
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Podgląd tabeli [" + WybranaTabela + "]", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            m_Dane = Nothing
            da = Nothing
            dt = Nothing
            HideScreen(False)
        End Try
    End Sub

    Private Sub ClearStatus()

        With Me
            With .tsslLicznik
                If .Enabled Then .Text = ""
            End With
            .tsslMain.Text = "Gotowe"
            .tspbMain.Value = 0
            .Cursor = Cursors.Default
        End With
        Application.DoEvents()
    End Sub

    Function DA_TI(ByVal Tabela As String, _
                  Optional ByVal Indeks As String = "", _
                  Optional ByVal Warunek As String = "") As OleDbDataAdapter

        Dim da As OleDbDataAdapter = New OleDbDataAdapter
        Dim cb As OleDbCommandBuilder
        Dim txt As String = "SELECT * FROM " + Tabela
        If Not String.IsNullOrEmpty(Warunek) Then txt += " WHERE " + Warunek
        If Not String.IsNullOrEmpty(Indeks) Then txt += " ORDER BY " + Indeks
        da.SelectCommand = New OleDbCommand(txt, Me.cns)
        cb = New OleDbCommandBuilder(da)
        da.InsertCommand = cb.GetInsertCommand
        da.UpdateCommand = cb.GetUpdateCommand
        da.DeleteCommand = cb.GetDeleteCommand
        cb = Nothing
        Return da
    End Function

    Private Sub FillStatusBar(Optional ByVal Max As Integer = 0, Optional ByVal Tekst As String = "")

        With Me
            With .tspbMain
                .Minimum = 0
                .Maximum = Max
                .Value = 0
            End With
            .tsslMain.Text = Tekst
            .Cursor = Cursors.WaitCursor
        End With
        Application.DoEvents()
    End Sub

    Private Sub HideScreen(Optional ByVal Ukryj As Boolean = True)

        If Ukryj Then
            With Me
                .Hide()
                .Cursor = Cursors.WaitCursor
            End With
            Return
        End If
        With Me
            .Cursor = Cursors.Default
            .tsslMain.Text = "Gotowe"
            .tspbMain.Value = 0
            .Visible = True
        End With
    End Sub

    Private Sub KomunikatShow()

        With Me.txtKomunikat
            .Size = New Size(360, 220)
            .Location = New Point(467, 189)
            .Anchor = AnchorStyles.Top And AnchorStyles.Right
            .Visible = True
        End With
    End Sub

#End Region ' Metody pomocnicze

 
    Private Sub tsmkEURO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmkEURO.Click
        Dim ans As String
        With My.Settings
            Try
                ans = InputBox("Aktualny kurs EURO", Me.Text, .KursEURO)
                If IsNumeric(ans) Then
                    .KursEURO = ans
                    .Save()
                Else
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            End Try
        End With
    End Sub

  
End Class
