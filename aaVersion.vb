﻿
' Aktualizacja wersji aplikacji

#Region "Opcje i referencje"

Imports System.Deployment.Application
Imports System.IO
Imports System.Reflection
Imports CM = ClHelpFul.CenterMsg

#End Region ' Opcje i referencje

' Modified 2015-01-13
Module aaVersion

    Friend CP As New ClHelpFul.Pomocne

    Sub pCheckVersion(Optional ByVal Publikacja As String = "", _
                      Optional ByVal KopiaZFolderu As String = "")

        Dim m_Assem As Assembly
        Dim m_App, m_Path As String
        Dim m_Date As DateTime
        Try
            m_Assem = Assembly.GetExecutingAssembly()
            m_Path = m_Assem.Location.ToString
            m_Date = My.Computer.FileSystem.GetFileInfo(m_Path).LastWriteTime
            With m_Path
                m_App = .Substring(.LastIndexOf(Path.DirectorySeparatorChar) + 1).Replace(".exe", ".application")
            End With
            If String.IsNullOrEmpty(Publikacja) Then
                Publikacja = ApplicationDeployment.CurrentDeployment.UpdateLocation.ToString
                With Publikacja
                    Publikacja = .Substring(.IndexOf("///") + 3).Replace("/", Path.DirectorySeparatorChar)
                End With
                Publikacja = Publikacja.Replace(Path.DirectorySeparatorChar + m_App, "")
            Else
                With Publikacja
                    If .EndsWith(Path.DirectorySeparatorChar) Then _
                        Publikacja = .Substring(0, .Length - 1)
                End With
            End If
            If Not String.IsNullOrEmpty(KopiaZFolderu) Then
                With KopiaZFolderu
                    If .EndsWith(Path.DirectorySeparatorChar) Then _
                        KopiaZFolderu = .Substring(0, .Length - 1)
                End With
                If CP.CompareFileDate(KopiaZFolderu + Path.DirectorySeparatorChar + m_App, m_Date, , True) Then Exit Try
                ' kopiowanie
                Dim tsource As Object
                Dim oSource As FileInfo
                Dim i As Integer
                Dim m_Folder, m_Tmp As String
                Try
                    With My.Computer.FileSystem
                        ' usuń wszystkie pliki i podfoldery z folderu docelowego
                        ' za wyjątkiem instalacyjnych
                        tsource = .GetFiles(Publikacja, FileIO.SearchOption.SearchAllSubDirectories)
                        For i = 0 To tsource.Count - 1
                            oSource = .GetFileInfo(tsource(i).ToString)
                            If oSource.FullName.IndexOf("dotnet") < 0 _
                                AndAlso oSource.FullName.IndexOf("windowsinstaller") < 0 _
                                AndAlso Not oSource.FullName.ToUpper.EndsWith("CHM") Then
                                .DeleteFile(oSource.FullName)
                                Try
                                    If .DirectoryExists(oSource.DirectoryName) Then
                                        .DeleteDirectory(oSource.DirectoryName, FileIO.DeleteDirectoryOption.ThrowIfDirectoryNonEmpty)
                                    End If
                                Catch ex As Exception
                                End Try
                            End If
                        Next i
                        tsource = .GetFiles(KopiaZFolderu, FileIO.SearchOption.SearchAllSubDirectories)
                        For i = 0 To tsource.Count - 1
                            oSource = .GetFileInfo(tsource(i).ToString)
                            If oSource.FullName.IndexOf("dotnet") < 0 AndAlso oSource.FullName.IndexOf("windowsinstaller") < 0 Then
                                m_Folder = oSource.DirectoryName.Replace(KopiaZFolderu, Publikacja)
                                If Not .DirectoryExists(m_Folder) Then .CreateDirectory(m_Folder)
                                m_Tmp = m_Folder + Path.DirectorySeparatorChar + oSource.Name
                                If .FileExists(m_Tmp) Then .DeleteFile(m_Tmp)
                                oSource.CopyTo(m_Tmp)
                            End If
                        Next i
                    End With
                Catch ex As Exception
                    Using New CM
                        Throw New ArgumentException("Kopiowanie plików" + vbCrLf + ex.Message)
                    End Using
                Finally
                    tsource = Nothing
                    oSource = Nothing
                End Try
            Else
                If CP.CompareFileDate(Publikacja + Path.DirectorySeparatorChar + m_App, m_Date, , True) Then Exit Try
            End If
            InstallUpdateSyncWithInfo(Publikacja)
        Catch ex As Exception
            Using New CM
                MessageBox.Show(ex.Message, "Kontrola wersji programu", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Using
        Finally
            m_Assem = Nothing
        End Try
    End Sub

    Private Sub InstallUpdateSyncWithInfo(ByVal SourcePath As String)

        Dim info As UpdateCheckInfo = Nothing
        If (ApplicationDeployment.IsNetworkDeployed) Then
            Dim AD As ApplicationDeployment = ApplicationDeployment.CurrentDeployment
            Try
                info = AD.CheckForDetailedUpdate()
            Catch dde As DeploymentDownloadException
                Using New CM
                    MessageBox.Show("Nowa wersja aplikacji nie może być teraz pobrana." + vbCrLf + _
                                    "Sprawdź dostęp do folderu aktualizacji lub spróbuj później. Błąd: " + dde.Message, _
                                    SourcePath, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
                Return
            Catch ioe As InvalidOperationException
                Using New CM
                    MessageBox.Show("Aplikacja nie może być aktualizowana. Błąd: " + ioe.Message, SourcePath, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Using
                Return
            End Try
            If (info.UpdateAvailable) Then
                Dim doUpdate As Boolean = True
                Dim dr As DialogResult
                If (Not info.IsUpdateRequired) Then
                    Using New CM
                        dr = MessageBox.Show("Dostępna nowa wersja aplikacji." + vbCrLf _
                                             + "Aktualizujesz teraz?", SourcePath, MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                    End Using
                    doUpdate = System.Windows.Forms.DialogResult.OK.Equals(dr)
                Else
                    Using New CM
                        MessageBox.Show("Aplikacja wykryła obowiązkową aktualizację do nowszej wersji." + vbCrLf _
                                        + "Nastąpi instalacja nowszej wersji.", SourcePath, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Using
                End If
                If (doUpdate) Then
                    Try
                        AD.Update()
                        Using New CM
                            MessageBox.Show("Aktualizacja zakończona pomyślnie. Aplikacja zostanie ponownie uruchomiona." + vbCrLf _
                                            + "Jeśli nie nastąpi to w ciągu kilkunastu sekund - uruchom ręcznie", SourcePath, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End Using
                        Application.Restart()
                    Catch dde As DeploymentDownloadException
                        Using New CM
                            MessageBox.Show("Aktualizacja do nowszej wersji nie powiodła się." + vbCrLf + "Sprawdź dostęp lub spróbuj później.", SourcePath, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End Using
                        Return
                    End Try
                End If
            End If
        End If
    End Sub

End Module
